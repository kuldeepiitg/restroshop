$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    set_active_menu();

    $(".rs-navbar .navbar-collapse ul li").click(function() {
    	if($(this).find("a").hasClass("active")) {
    		return;
    	}
    	$(".rs-navbar .navbar-collapse ul li").find("a").removeClass("active");
    	$(this).find("a").addClass("active");
    	set_active_menu();
    	$("html, body").animate({
            scrollTop: $("#"+this.id+"-start").offset().top - 50
        });
    });

    $(".terms-category li").click(function() {
    	if($(this).hasClass("rs-r")) {
    		return;
    	}
    	var e = this.id;
    	$(".terms-category li").removeClass("rs-r");
    	$(this).addClass("rs-r");
    	$("#terms-area, #faq-area, #privacy-area").slideUp("fast");
    	$("#"+e+"-area").slideDown("fast");
    });
});

function set_active_menu() {
	$(".rs-navbar .navbar-collapse ul li").find(".rs-border").remove();
	var e = $(".rs-navbar .navbar-collapse ul li").find(".active");
    var border = '</div><div class="rs-border center"></div><div class="clearfix">';
    e.html(border + e.html());
}