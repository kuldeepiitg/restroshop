package com.restroshop.volley;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import java.io.UnsupportedEncodingException;

/**
 * Submit JsonRequest, and handle string response.
 * <p/>
 * Created by kuldeep on 26/01/16.
 */
public class SubmitRequest extends JsonRequest<String> {

    public SubmitRequest(String url, String jsonRequestBody, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(jsonRequestBody == null ? Method.GET : Method.POST, url, jsonRequestBody, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        try {
            String string = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(string,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
