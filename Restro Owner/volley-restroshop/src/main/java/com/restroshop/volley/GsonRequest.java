package com.restroshop.volley;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Gson Request for volley, will return objects in response
 * of given class type.
 * <p/>
 * So no need to use Gson to convert, it is done in the request.
 * <p/>
 * Created by kuldeep on 23/01/16.
 */
public class GsonRequest<T> extends Request<T> {

    /**
     * Default charset for JSON request.
     */
    protected static final String PROTOCOL_CHARSET = "utf-8";

    /**
     * Gson to parse object from json to and fro
     */
    private static Gson gson;

    static {
        GsonBuilder builder = new GsonBuilder()
                .serializeNulls()
                .enableComplexMapKeySerialization()
                .setDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSSSSS");
        builder.registerTypeAdapter(Timestamp.class, new JsonDeserializer<Timestamp>() {
            @Override
            public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return Timestamp.valueOf(json.getAsString());
            }
        });

        gson = builder.create();
    }

    /**
     * Class to be used to make objects from json
     */
    private final Type clazz;
    /**
     * Http headers
     */
    private final Map<String, String> headers;
    /**
     * Listener for response
     */
    private final Listener<T> listener;
    /**
     * The request body to passed to resource
     */
    private String requestBody;

    /**
     * Gson POST request.
     * <p/>
     * It automatically chooses request type {@link Method#GET}/{@link Method#POST}
     * depending on request body. If request body is null then request type GET will be used
     * otherwise POST.
     *
     * @param url           of resource
     * @param requestBody   containing data to be passed to resource
     * @param clazz         class name of object expected in response
     * @param headers       headers to be passed with http request
     * @param listener      listeners to handle successful transaction
     * @param errorListener listener to handle failed transaction
     */
    private GsonRequest(String url, String requestBody, Type clazz, Map<String, String> headers,
                        Listener<T> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        this.clazz = clazz;
        this.headers = headers;
        this.listener = listener;
        this.requestBody = requestBody;
    }

    /**
     * Gson GET request.
     *
     * @param url           of resource
     * @param clazz         class type of object expected in response.
     * @param listener      listener to handle successful transaction
     * @param errorListener listener to handle failed transaction
     */
    public GsonRequest(String url, Type clazz,
                       Listener<T> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.clazz = clazz;
        this.headers = new HashMap<>();
        this.listener = listener;
    }

    /**
     * Gson POST request
     * <p/>
     * Content-Type in header is application/json
     *
     * @param url           of resource
     * @param requestBody   containing data to be passed to resource
     * @param clazz         class name of objects expected in response
     * @param listener      listeners to handle successful transaction
     * @param errorListener listener to handle error
     *                      <p/>
     *                      Note: clazz type should be consistent with T
     */
    public GsonRequest(String url, String requestBody, Type clazz,
                       Listener<T> listener, Response.ErrorListener errorListener) {
        this(url, requestBody, clazz,
                new HashMap<String, String>() {{
                    put("Content-Type", "application/json");
                }},
                listener, errorListener);
    }

    /**
     * Get {@link Gson} instance that is configured with deserializer and formats
     * for custom objects.
     *
     * @return Gson instance used by Request.
     */
    public static Gson getGson() {
        return gson;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {

        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return (Response<T>) Response.success(
                    gson.fromJson(json, clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    public byte[] getBody() {
        try {
            return requestBody == null ? null : requestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    requestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    /**
     * Insert key value pair into header.
     *
     * @param key   key for set/getting header element value
     * @param value value to be passed
     */
    public void putIntoHeader(String key, String value) {
        this.headers.put(key, value);
    }
}