package com.restroshop.listener;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * OnScrollChangeListener for paginated scrolling.
 *
 * It is similar to {@link RecyclerViewPaginationOnScrollListener}.
 *
 * Created by Kuldeep Yadav on 06/07/16.
 */
@TargetApi(Build.VERSION_CODES.M)
public abstract class RecyclerViewPaginationListener implements View.OnScrollChangeListener {

    /**
     * Minimum number of items left below, in order
     * to trigger load more.
     */
    private int visibleThreshold = 5;

    /**
     * Current page offset index of data that have been loaded
     */
    private int currentPage = 0;

    /**
     * Total number of items after last load
     */
    private int previousTotal = 0;

    /**
     * True, if loading in in progress
     */
    private boolean loading = true;

    /**
     * Recycler view layout manager
     */
    private RecyclerView.LayoutManager layoutManager;

    /**
     * Count of items to be fetched in each page.
     */
    private int pageSize = 10;

    public RecyclerViewPaginationListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public RecyclerViewPaginationListener(RecyclerView.LayoutManager layoutManager, int pageSize, int visibleThreshold) {
        this.layoutManager = layoutManager;
        this.pageSize = pageSize;
        this.visibleThreshold = visibleThreshold;
    }

    /**
     * Load more data to be shown in list.
     *
     * @param page  page number to compute start index
     * @param count count of items to be loaded.
     */
    public abstract void onLoadMore(int page, int count);

    @Override
    public void onScrollChange(View view, int i, int i1, int i2, int i3) {
        int lastVisibleItemPosition;
        int totalItemCount = layoutManager.getItemCount();

        lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();

        // If the total item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (totalItemCount < previousTotal) {

            this.currentPage = 0;
            this.previousTotal = totalItemCount;
            if (totalItemCount == 0) {
                this.loading = true;
            }
        }
        // If it’s still loading, we check to see if the dataset count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and total item count.
        if (loading && (totalItemCount > previousTotal)) {
            loading = false;
            previousTotal = totalItemCount;
        }

        // If it isn’t currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        // threshold should reflect how many total columns there are too
        if (!loading && (lastVisibleItemPosition + visibleThreshold) > totalItemCount) {
            currentPage++;
            onLoadMore(currentPage, pageSize);
            loading = true;
        }
    }
}
