package in.co.palup.android.utilities;

import android.content.Context;

import java.io.IOException;
import java.util.Properties;

/**
 * Configuration related utilities.
 * <p/>
 * Created by kuldeep on 07/07/16.
 */
public class ServerConfiguration {

    /**
     * Configuration properties
     */
    Properties properties;
    /**
     * Application context
     */
    private Context context;

    public ServerConfiguration(Context context) {
        this.context = context;
        properties = new Properties();
        try {
            properties.load(this.context.getAssets().open("restrosystem.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * @return hostname of server
     */
    public String getHostName() {
        return properties.getProperty("server-hostname");
    }

    /**
     * @return port number for server
     */
    public String getPort() {
        return properties.getProperty("server-port");
    }

    /**
     * @return authority (hostname:port)
     */
    public String getAuthority() {
        return getHostName() + ":" + getPort();
    }

    /**
     * @return protocol to be used, like http or https
     */
    public String getScheme() {
        return properties.getProperty("server-scheme");
    }

    public String getSystemBaseUrl() {
        return getScheme() + "://" + getAuthority();
    }

    public String getContentProviderBaseUrl() {
        return getScheme() + "://" + getAuthority() + "/content";
    }
}