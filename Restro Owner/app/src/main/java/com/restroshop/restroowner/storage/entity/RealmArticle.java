package com.restroshop.restroowner.storage.entity;

import com.restroshop.dao.model.Article;
import com.restroshop.dao.model.Unit;

import java.sql.Timestamp;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Realm equivalent to {@link Article}
 * <p/>
 * Created by kuldeep on 20/07/16.
 */
public class RealmArticle extends RealmObject {

    @PrimaryKey
    private long id;
    private String name;
    private long createdOn;
    private long lastUpdate;
    private String picUrl;
    private String description;
    private int unit;

    public RealmArticle() {
    }

    public RealmArticle(Article article) {
        this.name = article.getName();
        this.id = article.getId();
        this.createdOn = article.getCreatedOn().getTime();
        this.lastUpdate = (article.getLastUpdate() != null ? article.getLastUpdate().getTime() : -1);
        this.picUrl = article.getPicUrl();
        this.description = article.getDescription();
        this.unit = article.getUnit().ordinal();
    }

    /**
     * Convert a article list to realm article list, which is suitable to transact in Realm.
     *
     * @param articles list of {@link Article}s.
     * @return Realm list of articles.
     */
    public static RealmList<RealmArticle> toRealmArticleList(List<Article> articles) {
        RealmList<RealmArticle> realmArticles = new RealmList<>();
        for (Article article : articles) {
            realmArticles.add(new RealmArticle(article));
        }
        return realmArticles;
    }

    /**
     * Convert RealmArticle to Article.
     *
     * @return {@link Article} equivalent RealmArticle.
     */
    public Article toArticle() {
        Article article = new Article();
        article.setName(name);
        article.setDescription(description);
        article.setId(id);
        article.setCreatedOn(new Timestamp(createdOn));
        article.setLastUpdate(new Timestamp(lastUpdate));
        article.setPicUrl(picUrl);
        article.setUnit(Unit.values()[unit]);
        return article;
    }
}
