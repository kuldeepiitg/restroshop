package com.restroshop.restroowner.home;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.restroshop.dao.model.Location;
import com.restroshop.dao.model.SellingRate;
import com.restroshop.listener.RecyclerViewPaginationListener;
import com.restroshop.listener.RecyclerViewPaginationOnScrollListener;
import com.restroshop.restroowner.R;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.restroowner.decoration.DividerItemDecoration;
import com.restroshop.restroowner.storage.entity.RealmArticle;
import com.restroshop.volley.GsonRequest;
import com.restroshop.webservices.model.Basket;
import com.restroshop.webservices.model.Filter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import in.co.palup.android.utilities.ServerConfiguration;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Shopping fragment
 * <p/>
 * Created by kuldeep on 28/07/16.
 */
public class ShoppingFragment extends Fragment {

    /**
     * Arguments keys
     */
    public static String RATES_KEY = "rates";
    private static String ADAPTER_KEY = "adapter";
    /**
     * Size of page to be fetched for every server request
     */
    private final int PAGE_SIZE = 10;
    /**
     * Minimum number of items left in page, after which page loading will start.
     */
    private final int NEXT_PAGE_THRESHOLD = 3;
    /**
     * List of rates loaded
     */
    private List<SellingRate> rates;
    /**
     * UI components
     */
    private RecyclerView recyclerView;
    private AutoCompleteTextView filterTextView;
    private FloatingActionButton basketButton;
    /**
     * Selling rate adapter for recycler view
     */
    private SellingRateAdapter sellingRateAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setHasOptionsMenu(true);
        if (savedInstanceState != null) {
            this.rates = (List<SellingRate>) savedInstanceState.getSerializable(RATES_KEY);
            this.sellingRateAdapter = (SellingRateAdapter) savedInstanceState.getSerializable(ADAPTER_KEY);
        } else {
            this.rates = new ArrayList<>();
            this.sellingRateAdapter = new SellingRateAdapter(rates, getActivity());
            ShoppingBasket basket = (ShoppingBasket) ((RestroOwnerApplication) getActivity().getApplicationContext()).getBasket();
            basket.setOnChangeListener(new ShoppingBasket.OnDataChangeListener() {
                @Override
                public void onChange() {
                    setBasketButtonVisible();
                }
            });
        }
        init();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_shopping, container, false);
        this.filterTextView = (AutoCompleteTextView) rootView.findViewById(R.id.shopping_name_filter);
        this.recyclerView = (RecyclerView) rootView.findViewById(R.id.article_list);
        this.basketButton = (FloatingActionButton) rootView.findViewById(R.id.proceed_to_basket);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setItemAnimator(new DefaultItemAnimator());
        this.recyclerView.setAdapter(sellingRateAdapter);
        DividerItemDecoration divider = new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(divider);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.recyclerView.setOnScrollChangeListener(
                    new RecyclerViewPaginationListener((LinearLayoutManager) layoutManager, PAGE_SIZE, NEXT_PAGE_THRESHOLD) {
                        @Override
                        public void onLoadMore(int page, int count) {
                            loadNextPage(getFilter(), page * PAGE_SIZE, PAGE_SIZE);
                        }
                    });
        } else {
            this.recyclerView.setOnScrollListener(
                    new RecyclerViewPaginationOnScrollListener((LinearLayoutManager) layoutManager, PAGE_SIZE, NEXT_PAGE_THRESHOLD) {
                        @Override
                        public void onLoadMore(int page, int count) {
                            loadNextPage(getFilter(), page * PAGE_SIZE, PAGE_SIZE);
                        }
                    });
        }
        initFilter();
        initBasketButton();
        setBasketButtonVisible();
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_basket, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.empty_basket) {
            Basket basket = ((RestroOwnerApplication) getActivity().getApplicationContext()).getBasket();
            basket.empty();
            this.sellingRateAdapter.notifyDataSetChanged();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(ShoppingFragment.class.getName(), "on start");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // TODO: 14/08/16 Is it of some use to save adapter.
        outState.putSerializable(RATES_KEY, (ArrayList<SellingRate>) rates);
        outState.putSerializable(ADAPTER_KEY, sellingRateAdapter);
    }

    /**
     * Initialize fragment
     */
    private void init() {
        loadNextPage(getFilter(""), 0, PAGE_SIZE);
    }

    /**
     * Initialize article filter
     */
    private void initFilter() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<RealmArticle> results = realm.where(RealmArticle.class).findAll();
        Iterator<RealmArticle> iterator = results.iterator();
        Set<String> names = new HashSet<>();
        while (iterator.hasNext()) {
            names.add(iterator.next().toArticle().getName());
        }
        ArrayAdapter suggestionAdapter =
                new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_item, new ArrayList<>(names));
        filterTextView.setAdapter(suggestionAdapter);
        filterTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                rates.clear();
                loadNextPage(getFilter(), 0, PAGE_SIZE);
            }
        });
        filterTextView.addTextChangedListener(new TextWatcher() {

            private String lastText;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                lastText = String.valueOf(charSequence);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String filterText = filterTextView.getText().toString();
                if (!filterText.equals(lastText) && filterText.trim().equals("")) {
                    rates.clear();
                    loadNextPage(getFilter(), 0, PAGE_SIZE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    /**
     * Initialize checkout button function
     */
    private void initBasketButton() {
        basketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new BasketFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(RATES_KEY, (ArrayList) rates);
                fragment.setArguments(bundle);
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .addToBackStack("basket")
                        .commit();
            }
        });
        sellingRateAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                setBasketButtonVisible();
            }
        });
    }

    public void setBasketButtonVisible() {
        Basket basket = ((RestroOwnerApplication) getActivity().getApplicationContext()).getBasket();
        if (basket.getEntrySet().size() == 0) {
            basketButton.setVisibility(View.INVISIBLE);
        } else {
            basketButton.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Create filter
     *
     * @return current filter
     */
    private Filter getFilter(String namePrefix) {
        android.location.Location location = ((RestroOwnerApplication) getActivity().getApplicationContext()).getLastKnownLocation();
        Filter filter = new Filter();
        filter.setName(namePrefix);
        filter.setLocation(new Location(location.getLatitude(), location.getLongitude()));
        return filter;
    }

    /**
     * Get nameprefix from name filter view
     *
     * @return current filter
     */
    private Filter getFilter() {
        return getFilter(String.valueOf(filterTextView.getText()));
    }

    /**
     * Load selling rates.
     *
     * @param filter location of device
     * @param startIndex start index of page
     * @param count of items to be fetched
     */
    private void loadNextPage(Filter filter, int startIndex, int count) {
        ServerConfiguration configuration = ((RestroOwnerApplication) getActivity().getApplicationContext()).getConfiguration();
        String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/" +
                "restaurant/read/rates/filtered/" + startIndex + "/" + count;
        Gson gson = GsonRequest.getGson();
        GsonRequest<List<SellingRate>> request = new GsonRequest<>(url, gson.toJson(filter),
                new TypeToken<List<SellingRate>>() {
                }.getType(),
                new Response.Listener<List<SellingRate>>() {
                    @Override
                    public void onResponse(List<SellingRate> response) {
                        rates.addAll(response);
                        sellingRateAdapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(ShoppingFragment.class.getName(), error.toString());
                    }
                });
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        final String imei = telephonyManager.getDeviceId();
        request.putIntoHeader("imei", imei);
        RestroOwnerApplication.VolleyRequestQueueProvider.getInstance(getActivity().getApplicationContext()).getRequestQueue().add(request);
    }
}
