package com.restroshop.restroowner.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.restroshop.dao.model.SalePointArticle;
import com.restroshop.restroowner.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Adapter for article list recycler view.
 * <p/>
 * Created by Kuldeep Yadav on 06/07/16.
 */
public class ArticleListAdapter
        extends RecyclerView.Adapter<ArticleListAdapter.ArticleViewHolder>
        implements Filterable {

    /**
     * List of articles.
     */
    private List<SalePointArticle> articles;

    /**
     * List of filtered articles.
     */
    private List<SalePointArticle> filteredArticles;

    /**
     * Activity context
     */
    private Context context;

    public ArticleListAdapter(List<SalePointArticle> articles, Context context) {
        this.articles = articles;
        this.filteredArticles = new LinkedList<>();
        this.filteredArticles.addAll(articles);
        this.context = context;
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.article_list_row, parent, false);

        return new ArticleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        SalePointArticle article = filteredArticles.get(position);
        holder.name.setText(article.getName());
        holder.price.setText(article.getRate() + "/" + article.getUnit().name());
        String imgUrl = "http://192.168.1.80:8081/media/" + article.getPicUrl();

        // It is not so good idea to fetch image at this point
        Glide.with(context)
                .load(imgUrl)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return filteredArticles.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                ArrayList<SalePointArticle> list = new ArrayList<>();
                for (SalePointArticle article : articles) {
                    if (article.getName().toLowerCase().startsWith(((String) charSequence).toLowerCase())) {
                        list.add(article);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.count = list.size();
                filterResults.values = list;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredArticles = (List<SalePointArticle>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    /**
     * Article row view holder.
     */
    public class ArticleViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView price;
        public ImageView thumbnail;

        public ArticleViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.article_name);
            price = (TextView) view.findViewById(R.id.article_price);
            thumbnail = (ImageView) view.findViewById(R.id.article_thumbnail);
        }
    }
}