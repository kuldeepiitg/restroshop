package com.restroshop.restroowner.home;

import com.restroshop.webservices.model.Basket;

/**
 * Shopping basket is extended basket having some added utilities
 * on basket.
 * <p/>
 * Created by kuldeep on 16/08/16.
 */
public class ShoppingBasket extends Basket {

    /**
     * On basket data change listener
     */
    private OnDataChangeListener onChangeListener;

    @Override
    public void add(long articleId, int quantity) {
        super.add(articleId, quantity);
        if (onChangeListener != null) {
            onChangeListener.onChange();
        }
    }

    @Override
    public void increase(long articleId) {
        super.increase(articleId);
        if (onChangeListener != null) {
            onChangeListener.onChange();
        }
    }

    @Override
    public void decrease(long articleId) {
        super.decrease(articleId);
        if (onChangeListener != null) {
            onChangeListener.onChange();
        }
    }

    @Override
    public void remove(long articleId) {
        super.remove(articleId);
        if (onChangeListener != null) {
            onChangeListener.onChange();
        }
    }

    @Override
    public void empty() {
        super.empty();
        if (onChangeListener != null) {
            onChangeListener.onChange();
        }
    }

    public void setOnChangeListener(OnDataChangeListener onChangeListener) {
        this.onChangeListener = onChangeListener;
    }

    /**
     * Listener on basket data changes like add, remove,
     * increase, decrease and empty.
     */
    public interface OnDataChangeListener {
        void onChange();
    }
}
