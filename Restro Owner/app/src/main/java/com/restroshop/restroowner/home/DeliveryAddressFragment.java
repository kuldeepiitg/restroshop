package com.restroshop.restroowner.home;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.restroshop.dao.model.Order;
import com.restroshop.dao.model.Restaurant;
import com.restroshop.dao.model.Session;
import com.restroshop.restroowner.R;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.restroowner.storage.SessionManager;
import com.restroshop.volley.GsonRequest;
import com.restroshop.volley.SubmitRequest;

import in.co.palup.android.utilities.ServerConfiguration;

/**
 * Postal address form of a delivery point.
 * <p/>
 * Created by kuldeep on 11/08/16.
 */
public class DeliveryAddressFragment extends Fragment {

    /**
     * Form UI components
     */
    private EditText personName;
    private EditText phoneNumber;
    private EditText building;
    private EditText street;
    private EditText area;
    private EditText city;
    private EditText state;
    private EditText pinCode;
    private View rootView;

    /**
     * Server configuration
     */
    private ServerConfiguration configuration;

    /**
     * Restaurant details
     */
    private Restaurant restaurant;

    /**
     * Order to be made
     */
    private Order order;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configuration = ((RestroOwnerApplication) getActivity()
                .getApplicationContext())
                .getConfiguration();

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        final Bundle args = getArguments();
        // FIXME: 14/08/16 order is taken from arguments, a better approach is take from persistant storage
        order = (Order) args.getSerializable("order");
        rootView = inflater.inflate(R.layout.fragment_delivery_address, container, false);
        TextView itemCountView = (TextView) rootView.findViewById(R.id.basket_item_count);
        TextView totalAmount = (TextView) rootView.findViewById(R.id.basket_total);

        personName = (EditText) rootView.findViewById(R.id.person_name);
        phoneNumber = (EditText) rootView.findViewById(R.id.phone);
        building = (EditText) rootView.findViewById(R.id.building);
        street = (EditText) rootView.findViewById(R.id.street);
        area = (EditText) rootView.findViewById(R.id.area);
        city = (EditText) rootView.findViewById(R.id.city);
        state = (EditText) rootView.findViewById(R.id.state);
        pinCode = (EditText) rootView.findViewById(R.id.pincode);

        fillForm();

        Resources resources = getResources();
        String itemCountText = String.format(resources.getString(R.string.items), order.readItemCount());
        itemCountView.setText(itemCountText);

        String basketCostText = String.format(resources.getString(R.string.basket_total), order.readBasketCost());
        totalAmount.setText(basketCostText);

        final View basketCheckout = rootView.findViewById(R.id.basket_checkout);
        basketCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {

                    updateRestaurantDetails();
                    setOrderDeliveryAddress();
                    args.putSerializable("order", order);
                    PaymentFragment fragment = new PaymentFragment();
                    fragment.setArguments(args);
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_frame, fragment)
                            .addToBackStack("payment_fragment")
                            .commit();
                }
            }
        });

        return rootView;
    }

    /**
     * Update restaurant with new details.
     * <p/>
     * Asynchronous
     */
    public void updateRestaurantDetails() {
        if (phoneNumber.getText() != null && phoneNumber.getText().length() != 0) {
            restaurant.setPhone(phoneNumber.getText().toString());
        }
        if (building.getText() != null && building.getText().length() != 0) {
            restaurant.setComplexName(building.getText().toString());
        }
        if (street.getText() != null && street.getText().length() != 0) {
            restaurant.setStreet(street.getText().toString());
        }
        if (area.getText() != null && area.getText().length() != 0) {
            restaurant.setArea(area.getText().toString());
        }
        if (city.getText() != null && city.getText().length() != 0) {
            restaurant.setCity(city.getText().toString());
        }
        if (state.getText() != null && street.getText().length() != 0) {
            restaurant.setState(state.getText().toString());
        }
        if (pinCode.getText() != null && pinCode.getText().length() != 0) {
            restaurant.setPincode(pinCode.getText().toString());
        }

        String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/" +
                "restaurant/update/";
        Gson gson = GsonRequest.getGson();
        SubmitRequest request = new SubmitRequest(url,
                gson.toJson(restaurant),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(DeliveryAddressFragment.class.getName(), response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(DeliveryAddressFragment.class.getName(), error.toString());
            }
        });
        RestroOwnerApplication
                .VolleyRequestQueueProvider
                .getInstance(getActivity().getApplicationContext())
                .getRequestQueue()
                .add(request);
    }

    /**
     * Fill delivery details in order
     */
    private void setOrderDeliveryAddress() {
        if (phoneNumber.getText() != null && phoneNumber.getText().length() != 0) {
            order.setPhone(phoneNumber.getText().toString());
        }
        if (building.getText() != null && building.getText().length() != 0) {
            order.setComplex(building.getText().toString());
        }
        if (street.getText() != null && street.getText().length() != 0) {
            order.setStreet(street.getText().toString());
        }
        if (area.getText() != null && area.getText().length() != 0) {
            order.setArea(area.getText().toString());
        }
        if (city.getText() != null && city.getText().length() != 0) {
            order.setCity(city.getText().toString());
        }
        if (state.getText() != null && street.getText().length() != 0) {
            order.setState(state.getText().toString());
        }
        if (pinCode.getText() != null && pinCode.getText().length() != 0) {
            order.setPincode(pinCode.getText().toString());
        }
    }

    /**
     * Fill the form with previous address that is present on server.
     */
    private void fillForm() {

        SessionManager sessionManager = ((RestroOwnerApplication) getActivity().getApplicationContext()).getSessionManager();
        Session session = sessionManager.getSession();
        long restroId = session.getUserId();

        ServerConfiguration configuration = ((RestroOwnerApplication) getActivity().getApplicationContext()).getConfiguration();
        String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/" +
                "restaurant/read/id/";
        GsonRequest<Restaurant> request = new GsonRequest<>(url, Long.toString(restroId),
                new TypeToken<Restaurant>() {
                }.getType(), new Response.Listener<Restaurant>() {
            @Override
            public void onResponse(Restaurant response) {
                restaurant = response;
                if (response.getName() != null)
                    personName.setText(response.getName());
                if (response.getPhone() != null)
                    phoneNumber.setText(response.getPhone());
                if (response.getComplexName() != null)
                    building.setText(response.getComplexName());
                if (response.getStreet() != null)
                    street.setText(response.getStreet());
                if (response.getArea() != null)
                    area.setText(response.getArea());
                if (response.getCity() != null) {
                    city.setText(response.getCity());
                }
                if (response.getState() != null) {
                    state.setText(response.getState());
                }
                if (response.getPincode() != null) {
                    pinCode.setText(response.getPincode());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(DeliveryAddressFragment.class.getName(), error.toString());
            }
        });
        RestroOwnerApplication.VolleyRequestQueueProvider
                .getInstance(getActivity().getApplicationContext())
                .getRequestQueue()
                .add(request);
    }

    /**
     * Validate for mandatory fields.
     *
     * @return true if all mandatory fields are filled, false otherwise.
     */
    private boolean validate() {
        if (personName.getText() == null || personName.getText().length() == 0) {
            Snackbar.make(rootView, "Person name can't be null. \n " +
                    "Fields marked with * are mandatory.", Snackbar.LENGTH_LONG).show();
            return false;
        } else if (phoneNumber.getText() == null || phoneNumber.getText().length() == 0) {
            Snackbar.make(rootView, "Phone number can't be null. \n " +
                    "Fields marked with * are mandatory.", Snackbar.LENGTH_LONG).show();
            return false;
        } else if (area.getText() == null || area.getText().length() == 0) {
            Snackbar.make(rootView, "Area can't be null. \n " +
                    "Fields marked with * are mandatory.", Snackbar.LENGTH_LONG).show();
            return false;
        } else if (pinCode.getText() == null || pinCode.getText().length() == 0) {
            Snackbar.make(rootView, "PIN code can't be null. \n " +
                    "Fields marked with * are mandatory.", Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
