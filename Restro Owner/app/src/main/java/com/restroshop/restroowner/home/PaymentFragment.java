package com.restroshop.restroowner.home;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.restroshop.dao.model.Order;
import com.restroshop.restroowner.R;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.volley.GsonRequest;
import com.restroshop.volley.SubmitRequest;

import in.co.palup.android.utilities.ServerConfiguration;

/**
 * Payment options available.
 * <p/>
 * Created by kuldeep on 11/08/16.
 */
public class PaymentFragment extends Fragment {

    /**
     * Server configuration
     */
    private ServerConfiguration configuration;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configuration = ((RestroOwnerApplication) getActivity()
                .getApplicationContext())
                .getConfiguration();

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle args = getArguments();
        final Order order = (Order) args.getSerializable(BasketFragment.ORDER_KEY);
        View rootView = inflater.inflate(R.layout.fragment_payment, container, false);
        TextView itemCountView = (TextView) rootView.findViewById(R.id.basket_item_count);
        TextView totalAmount = (TextView) rootView.findViewById(R.id.basket_total);

        Resources resources = getResources();
        String itemCountText = String.format(resources.getString(R.string.items), order.readItemCount());
        itemCountView.setText(itemCountText);

        String basketCostText = String.format(resources.getString(R.string.basket_total), order.readBasketCost());
        totalAmount.setText(basketCostText);

        final View basketCheckout = rootView.findViewById(R.id.basket_checkout);
        basketCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // send order
                String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/" +
                        "restaurant/order/";
                Gson gson = GsonRequest.getGson();
                SubmitRequest request = new SubmitRequest(url,
                        gson.toJson(order),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.i(PaymentFragment.class.getName(), response);
                                Fragment successfulOrderFragment = new SuccessfulOrderFragment();
                                getFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame, successfulOrderFragment)
                                        .commit();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(PaymentFragment.class.getName(), error.toString());
                    }
                });
                RestroOwnerApplication
                        .VolleyRequestQueueProvider
                        .getInstance(getActivity().getApplicationContext())
                        .getRequestQueue()
                        .add(request);
            }
        });

        return rootView;
    }
}
