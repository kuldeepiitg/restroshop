package com.restroshop.restroowner.storage;

import com.restroshop.restroowner.storage.entity.RealmBasketSelection;
import com.restroshop.webservices.model.Basket;

import java.util.Map;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Basket manager.
 * Adds, deletes, get and updates article quantities in basket.
 * <p>
 * Created by kuldeep on 08/08/16.
 */
public class BasketManager {

    private static BasketManager basketManager;
    private Realm realm;


    private BasketManager() {
        realm = Realm.getDefaultInstance();
    }

    /**
     * Get instance of basket manager.
     *
     * @return basket manager instance
     */
    public static BasketManager getInstance() {
        if (basketManager == null) {
            basketManager = new BasketManager();
        }
        return basketManager;
    }

    /**
     * Add article to basket with given quantity.
     *
     * @param articleId id of article.
     * @param quantity  quantity of article.
     */
    public void add(long articleId, int quantity) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(new RealmBasketSelection(articleId, quantity));
        realm.commitTransaction();
    }

    /**
     * Remove article from basket.
     *
     * @param articleId id of article.
     */
    public void remove(long articleId) {
        realm.beginTransaction();
        realm.where(RealmBasketSelection.class)
                .equalTo("articleId", articleId)
                .findFirst()
                .deleteFromRealm();
        realm.commitTransaction();
    }

    /**
     * Get basket quantity of article.
     *
     * @param articleId id of article
     * @return quantity of article
     */
    public int get(long articleId) {

        RealmBasketSelection selection = realm.where(RealmBasketSelection.class)
                .equalTo("articleId", articleId)
                .findFirst();
        if (selection == null) {
            return 0;
        }
        return selection.getQuantity();
    }

    /**
     * Convert realm basket to {@link Basket}
     *
     * @return Basket containing all selection
     */
    public Basket toBasket() {
        Basket basket = new Basket();
        RealmResults<RealmBasketSelection> selections
                = realm.where(RealmBasketSelection.class).findAll();
        for (int i = 0; i < selections.size(); i++) {
            RealmBasketSelection selection = selections.get(i);
            if (selection.getQuantity() > 0) {
                basket.add(selection.getArticleId(), selection.getQuantity());
            }
        }
        return basket;
    }

    /**
     * Load basket in realm db
     *
     * @param basket basket to be loaded
     */
    public void loadRealmBasket(Basket basket) {
        Set<Map.Entry<Long, Integer>> set = basket.getEntrySet();
        for (Map.Entry<Long, Integer> entry : set) {
            add(entry.getKey(), entry.getValue());
        }
    }
}
