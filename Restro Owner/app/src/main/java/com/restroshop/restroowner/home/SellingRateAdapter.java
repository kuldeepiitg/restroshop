package com.restroshop.restroowner.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.restroshop.components.CountWidget;
import com.restroshop.dao.model.Article;
import com.restroshop.dao.model.SellingRate;
import com.restroshop.restroowner.R;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.webservices.model.Basket;

import java.io.Serializable;
import java.util.List;

import in.co.palup.android.utilities.ServerConfiguration;

/**
 * Adapter for article/selling rate recycler view.
 * <p/>
 * Created by Kuldeep Yadav on 06/08/16.
 */
public class SellingRateAdapter
        extends RecyclerView.Adapter<SellingRateAdapter.ArticleViewHolder>
        implements Serializable {

    /**
     * List of selling rates to be shown
     */
    private List<SellingRate> rates;

    /**
     * Activity context
     */
    private transient Context context;

    /**
     * Server configuration
     */
    private transient ServerConfiguration configuration;

    /**
     * Basket
     */
    private transient Basket basket;

    public SellingRateAdapter(List<SellingRate> rates, Context context) {
        this.rates = rates;
        this.context = context;
        this.configuration =
                ((RestroOwnerApplication) context.getApplicationContext()).getConfiguration();
        this.basket = ((RestroOwnerApplication) context.getApplicationContext()).getBasket();
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.article_list_row, parent, false);

        ArticleViewHolder viewHolder = new ArticleViewHolder(itemView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, final int position) {
        final SellingRate rate = rates.get(position);
        holder.name.setText(rate.getArticle().getName());
        holder.price.setText(rate.getPrice() + "/" + rate.getArticle().getUnit().name());
        String imgUrl = configuration.getContentProviderBaseUrl() + "/media/"
                + rate.getArticle().getPicUrl();

        // It is not so good idea to fetch image at this point
        Glide.with(context)
                .load(imgUrl)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.thumbnail);

        holder.counter.setCount(basket.getQuantity(rate.getArticle().getId()));
        // FIXME: 10/08/16 use OnCountChangeListener in place of OnButtonClickListener as
        // FIXME: it wont support input by keyboard
        holder.counter.setOnButtonClickListener(new CountWidget.OnButtonClickListener() {
            @Override
            public void OnPlusButtonClick(int count) {
                Article article = rates.get(position).getArticle();
                basket.add(article.getId(), count);
            }

            @Override
            public void OnMinusButtonClick(int count) {
                Article article = rates.get(position).getArticle();
                basket.add(article.getId(), count);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rates.size();
    }

    /**
     * Article row view holder.
     */
    public static class ArticleViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView price;
        public ImageView thumbnail;
        public CountWidget counter;

        public ArticleViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.article_name);
            price = (TextView) view.findViewById(R.id.article_price);
            thumbnail = (ImageView) view.findViewById(R.id.article_thumbnail);
            counter = (CountWidget) view.findViewById(R.id.counter);
        }
    }
}
