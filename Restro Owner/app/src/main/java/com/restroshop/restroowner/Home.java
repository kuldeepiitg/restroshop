package com.restroshop.restroowner;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.restroshop.restroowner.home.ShoppingFragment;
import com.restroshop.restroowner.storage.ArticleManager;

/**
 * Home for a restaurant owner.
 * <p/>
 * It will have view customized for home
 * depending on his behaviour.
 */
public class Home extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawer;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private String[] drawerListNames;

    /**
     * The article manager
     */
    private ArticleManager articleManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.dashboard_toolbar);
        setSupportActionBar(toolbar);

        drawerListNames = getResources().getStringArray(R.array.drawer_list);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawer = (LinearLayout) findViewById(R.id.left_drawer);
        mDrawerList = (ListView) mDrawerLayout.findViewById(R.id.drawer_list);

        addDrawer();
        init();
    }

    /**
     * Add drawer to dashboard home activity.
     */
    private void addDrawer() {
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, drawerListNames));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final ImageView navDrawerToggle = (ImageView) findViewById(R.id.nav_drawer_toggle);
        navDrawerToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(navDrawerToggle.getWindowToken(), 0);
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    /**
     * Initialize home screen
     */
    private void init() {
        synchronizeArticles();
    }

    /**
     * Synchronize articles
     */
    private void synchronizeArticles() {
        RestroOwnerApplication restroOwnerApplicationContext =
                (RestroOwnerApplication) this.getApplicationContext();
        this.articleManager = restroOwnerApplicationContext.getArticleManager();
        articleManager.setOnSyncCompletionListener(new ArticleManager.OnSyncCompletionListener() {
            @Override
            public void onSyncCompletion() {
                goTo("browse");
            }
        });
        articleManager.syncArticles();
    }

    /**
     * Take action depending on the navigation drawer item clicked
     *
     * @param drawerItemName drawer item name
     */
    private void goTo(String drawerItemName) {

        // It might have been hidden if call is coming after splash screen
        // Because splash screen doesn't need action bar, it is set to be hidden.
        if (!getSupportActionBar().isShowing()) getSupportActionBar().show();

        Log.i("GoTo", drawerItemName);
        Fragment fragment = null;
        if (drawerItemName.toLowerCase().equals("dashboard")) {
            mDrawerList.setItemChecked(0, true);
            return;
        } else if (drawerItemName.toLowerCase().equals("quick buy")) {
            mDrawerList.setItemChecked(1, true);
            return;
        } else if (drawerItemName.toLowerCase().equals("smart buy")) {
            mDrawerList.setItemChecked(2, true);
            return;
        } else if (drawerItemName.toLowerCase().equals("browse")) {
            fragment = new ShoppingFragment();
            mDrawerList.setItemChecked(3, true);
        } else {
            throw new RuntimeException("Invalid fragment name");
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack("shopping")
                .commit();
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    /**
     * Drawer item click listener
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            goTo(drawerListNames[position]);
        }
    }
}
