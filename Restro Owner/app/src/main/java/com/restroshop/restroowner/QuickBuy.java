package com.restroshop.restroowner;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.restroshop.dao.model.SalePointArticle;
import com.restroshop.listener.RecyclerViewPaginationListener;
import com.restroshop.listener.RecyclerViewPaginationOnScrollListener;
import com.restroshop.restroowner.adapter.ArticleListAdapter;
import com.restroshop.restroowner.decoration.DividerItemDecoration;
import com.restroshop.restroowner.model.ArticleName;
import com.restroshop.volley.GsonRequest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import in.co.palup.android.utilities.ServerConfiguration;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Quick buy activity.
 */
public class QuickBuy extends AppCompatActivity {

    /**
     * Size of page to be fetched.
     */
    private static final int PAGE_SIZE = 10;
    /**
     * List of articles loaded
     */
    ArrayList<SalePointArticle> articles = new ArrayList<>();
    /**
     * Recycler view to show article list
     */
    private RecyclerView mRecyclerView;

    /**
     * Filter text view.
     */
    private AutoCompleteTextView filterTextView;

    /**
     * Adapter for article recycler view
     */
    private ArticleListAdapter articleListAdapter;

    /**
     * Server configuration
     */
    private ServerConfiguration configuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_buy);
        configuration = new ServerConfiguration(this);
        filterTextView = (AutoCompleteTextView) findViewById(R.id.quick_buy_filter);

        Realm realm = Realm.getDefaultInstance();
        RealmResults<ArticleName> results = realm.where(ArticleName.class).findAll();
        Iterator<ArticleName> iterator = results.iterator();
        List<String> names = new ArrayList<>();
        while (iterator.hasNext()) {
            names.add(iterator.next().getName());
        }
        ArrayAdapter suggestionAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, names);
        filterTextView.setAdapter(suggestionAdapter);

        mRecyclerView = (RecyclerView) findViewById(R.id.article_list);
        articleListAdapter = new ArticleListAdapter(articles, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(articleListAdapter);
        DividerItemDecoration divider = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(divider);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mRecyclerView.setOnScrollChangeListener(new RecyclerViewPaginationListener((LinearLayoutManager) layoutManager, 10, 3) {
                @Override
                public void onLoadMore(int page, int count) {
                    loadNextPage(page, count);
                }
            });
        } else {
            mRecyclerView.setOnScrollListener(new RecyclerViewPaginationOnScrollListener((LinearLayoutManager) layoutManager, 10, 3) {
                @Override
                public void onLoadMore(int page, int count) {
                    loadNextPage(page, count);
                }
            });
        }
        initializeList();
        initializeFilter();

    }

    private void initializeFilter() {
        filterTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                articleListAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    /**
     * Initialize list with first page
     */
    private void initializeList() {
        loadNextPage(0, PAGE_SIZE);
    }

    /**
     * Load next page of articles.
     *
     * @param page page number that have been fetched
     * @param pageSize number of items in a page
     */
    private void loadNextPage(int page, int pageSize) {
        int startIndex = page * pageSize;

        String url = configuration.getScheme() + "://" + configuration.getAuthority() +
                "/article/read/available/" + startIndex + "/" + pageSize;
        GsonRequest<List<SalePointArticle>> articleListRequest =
                new GsonRequest<>(url, null,
                        new TypeToken<List<SalePointArticle>>() {
                        }.getType(),
                        new Response.Listener<List<SalePointArticle>>() {
                            @Override
                            public void onResponse(List<SalePointArticle> response) {
                                Log.i("QuickBuy", String.valueOf(response.size()));
                                articles.addAll(response);
                                articleListAdapter.notifyDataSetChanged();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Quick Buy", error.getLocalizedMessage());
                    }
                });
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        articleListRequest.putIntoHeader("imei", telephonyManager.getDeviceId());

        SharedPreferences sharedPreferences = this.getSharedPreferences(LoginActivity.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        long id = sharedPreferences.getLong(LoginActivity.ID_KEY, -1L);
        articleListRequest.putIntoHeader("userid", String.valueOf(id));
        RestroOwnerApplication.VolleyRequestQueueProvider.getInstance(getApplicationContext()).getRequestQueue().add(articleListRequest);
    }
}