package com.restroshop.restroowner.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.restroshop.components.CountWidget;
import com.restroshop.dao.model.SellingRate;
import com.restroshop.restroowner.R;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.webservices.model.Basket;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import in.co.palup.android.utilities.ServerConfiguration;

/**
 * Basket articles adapter. List of articles in basket is given by this adapter.
 * <p>
 * Created by kuldeep on 08/08/16.
 */
public class BasketArticlesAdapter
        extends RecyclerView.Adapter<BasketArticlesAdapter.BasketArticleViewHolder> {

    private Basket basket;
    private List<Map.Entry<Long, Integer>> selections;
    private Map<Long, SellingRate> rates;
    private Context context;
    private OnAnyChangeListener onAnyChangeListener;

    /**
     * Server configuration
     */
    private ServerConfiguration configuration;

    public BasketArticlesAdapter(Map<Long, SellingRate> rates,
                                 Context context) {
        this.basket = ((RestroOwnerApplication) context).getBasket();
        this.selections = new ArrayList<>();
        this.selections.addAll(basket.getEntrySet());
        this.rates = rates;
        this.context = context;
        this.configuration =
                ((RestroOwnerApplication) context.getApplicationContext())
                        .getConfiguration();
        this.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                selections = new ArrayList<>();
                selections.addAll(basket.getEntrySet());
            }
        });
    }

    @Override
    public BasketArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.article_list_row, parent, false);
        BasketArticleViewHolder viewHolder = new BasketArticleViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BasketArticleViewHolder holder, final int position) {
        long articleId = selections.get(position).getKey();
        SellingRate rate = rates.get(articleId);
        holder.name.setText(rate.getArticle().getName());
        holder.price.setText(rate.getPrice() + "/" + rate.getArticle().getUnit().name());
        String imgUrl = configuration.getContentProviderBaseUrl() + "/media/"
                + rate.getArticle().getPicUrl();

        // It is not so good idea to fetch image at this point
        Glide.with(context)
                .load(imgUrl)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.thumbnail);

        holder.counter.setCount(selections.get(position).getValue());
        holder.counter.setOnButtonClickListener(new CountWidget.OnButtonClickListener() {
            @Override
            public void OnPlusButtonClick(int count) {
                basket.add(selections.get(position).getKey(), count);
                selections.get(position).setValue(count);
                if (onAnyChangeListener != null) {
                    onAnyChangeListener.update(basket.getEntrySet().size(), getBasketCost());
                }
            }

            @Override
            public void OnMinusButtonClick(int count) {
                if (count == 0) {
                    basket.remove(selections.get(position).getKey());
                    selections.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, selections.size());
                } else {
                    basket.add(selections.get(position).getKey(), count);
                    selections.get(position).setValue(count);
                }
                if (onAnyChangeListener != null) {
                    onAnyChangeListener.update(basket.getEntrySet().size(), getBasketCost());
                }
            }
        });
    }

    /**
     * Get total price of basket.
     *
     * @return price of basket
     */
    public int getBasketCost() {
        int cost = 0;
        for (Map.Entry<Long, Integer> entry : basket.getEntrySet()) {
            cost += entry.getValue() * rates.get(entry.getKey()).getPrice();
        }
        return cost;
    }

    @Override
    public int getItemCount() {
        return selections.size();
    }

    /**
     * Set listener for any change. It will be called when any change happens to adapter
     *
     * @param onAnyChangeListener the listener
     */
    public void setOnAnyChangeListener(OnAnyChangeListener onAnyChangeListener) {
        this.onAnyChangeListener = onAnyChangeListener;
    }

    /**
     * Listener on any change
     */
    public interface OnAnyChangeListener {

        /**
         * Call update on any change in adapter
         *
         * @param itemCount count of items in basket
         * @param totalCost total cost of basket
         */
        void update(int itemCount, int totalCost);
    }

    /**
     * Basket article item view holder
     */
    public static class BasketArticleViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView price;
        public ImageView thumbnail;
        public CountWidget counter;

        public BasketArticleViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.article_name);
            price = (TextView) itemView.findViewById(R.id.article_price);
            thumbnail = (ImageView) itemView.findViewById(R.id.article_thumbnail);
            counter = (CountWidget) itemView.findViewById(R.id.counter);
        }
    }
}
