package com.restroshop.restroowner.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * ArticleName
 * <p/>
 * Created by kuldeep on 17/07/16.
 */
public class ArticleName extends RealmObject {

    /**
     * Name of article
     */
    @PrimaryKey
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
