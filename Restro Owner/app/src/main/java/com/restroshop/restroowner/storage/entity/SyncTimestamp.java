package com.restroshop.restroowner.storage.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * We are storing data in client so that we don't have to fetch that
 * every time. We need to sync data if data on server side is modified,
 * for that we need a track of time stamp.
 * <p/>
 * Created by kuldeep on 17/07/16.
 */
public class SyncTimestamp extends RealmObject {

    /**
     * Type of data synced. It is class name of the data saved.
     * For example, If I am storing Articles on client side then I will have my type
     * name as Article.
     */
    @PrimaryKey
    private String type;

    /**
     * Time stamp, when object of {@link SyncTimestamp#type} were synced.
     */
    private long time;

    public SyncTimestamp(String type) {
        this.type = type;
        this.time = System.currentTimeMillis();
    }

    public SyncTimestamp() {
    }

    /**
     * Get timestamp.
     *
     * @return timestamp
     */
    public long getTime() {
        return time;
    }

    /**
     * Set timestamp.
     *
     * @param time timestamp to set
     */
    public void setTime(long time) {
        this.time = time;
    }

    /**
     * Get type with which time stamp is associated.
     *
     * @return type with which time stamp is associated.
     */
    public String getType() {
        return type;
    }
}
