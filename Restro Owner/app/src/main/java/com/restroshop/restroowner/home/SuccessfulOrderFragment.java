package com.restroshop.restroowner.home;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.restroshop.restroowner.R;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.webservices.model.Basket;

/**
 * Summary of successful order.
 * <p/>
 * Created by kuldeep on 13/08/16.
 */
public class SuccessfulOrderFragment extends Fragment {

    /**
     * UI components
     */
    private Button okButton;

    /**
     * Time out to switch to dashboard.
     */
    private long TIME_OUT = 5 * 1000;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_order_successful, container, false);
        okButton = (Button) rootView.findViewById(R.id.button_ok);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                goToShoppingFragment();
            }
        };

        final Handler handler = new Handler();
        handler.postDelayed(runnable, TIME_OUT);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(runnable);
                Basket basket = ((RestroOwnerApplication) getActivity()
                        .getApplicationContext())
                        .getBasket();
                basket.empty();
                goToShoppingFragment();
            }
        });
    }

    /**
     * Go to dashboard.
     */
    private void goToShoppingFragment() {
        Fragment fragment = new ShoppingFragment();
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }
}
