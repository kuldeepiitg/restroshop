package com.restroshop.restroowner;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.restroshop.dao.model.Restaurant;
import com.restroshop.restroowner.model.ArticleName;
import com.restroshop.restroowner.storage.ArticleManager;
import com.restroshop.volley.GsonRequest;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.regex.Pattern;

import in.co.palup.android.utilities.ServerConfiguration;
import io.realm.Realm;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    /**
     * Signin request code to identify response
     */
    private static final int RC_SIGN_IN = 9001;

    /**
     * Logging tag
     */
    private static final String TAG = "Login";
    /**
     * Shared preference instance name
     */
    public static String SHARED_PREFERENCE_NAME = "Login";
    /**
     * Key to save id in shared preference
     */
    public static String ID_KEY = "id";
    Realm realm;
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    /**
     * Google API Client
     */
    private GoogleApiClient mGoogleApiClient;
    /**
     * Google API client connection callback
     */
    private GoogleApiClient.ConnectionCallbacks mConnectionCallbacks;
    /**
     * Google API client connection failed listener
     */
    private GoogleApiClient.OnConnectionFailedListener mConnectionFailedListener;
    /**
     * Server configuration
     */
    private ServerConfiguration configuration;
    /**
     * Gson object serializer and deserializer.
     */
    private Gson gson = GsonRequest.getGson();
    /**
     * Shared preference to store session
     */
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        configuration = new ServerConfiguration(this);
        sharedPreferences = this.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);

        realm = Realm.getDefaultInstance();

        initEmailPasswordLogin();
        initGoogleLogin();
    }

/********************************** Login With Email & Password ***********************************/

    /**
     * Login with email and password.
     */
    private void initEmailPasswordLogin() {
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        /**
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                attemptLogin();
                goToDashboard();
            }
        });
         */

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    /**
     * Check if email is valid
     *
     * @param email email of user
     * @return true if it is a valid email id, false otherwise
     */
    private boolean isEmailValid(String email) {
        final String pattern =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        return Pattern.matches(pattern, email);
    }

    /**
     * Check if password fulfills validity criteria
     *
     * @param password password of user
     * @return true if password satisfies necessity, false otherwise
     */
    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Login with google
     */
    private void initGoogleLogin() {

        // Initializing google plus api client
        mConnectionCallbacks = new LoginConnectionCallbacks();
        mConnectionFailedListener = new LoginOnConnectionFailedListener();
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(mConnectionCallbacks)
                .addOnConnectionFailedListener(mConnectionFailedListener)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        findViewById(R.id.btn_sign_in_with_google).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
    }

/******************************************** Login With Google ***********************************/

    /**
     * Sign in
     */
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    /**
     * Handle signin result
     *
     * @param result containing account info
     */
    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount account = result.getSignInAccount();

            final Restaurant restaurant = new Restaurant();
            assert account != null;
            restaurant.setEmail(account.getEmail());
            String url = configuration.getScheme() + "://" + configuration.getAuthority() +
                    "/restaurant/session/open";

            GsonRequest<Restaurant> request =
                    new GsonRequest<>(url, gson.toJson(restaurant),
                            new TypeToken<Restaurant>() {
                            }.getType(),
                            new Response.Listener<Restaurant>() {
                                @Override
                                public void onResponse(Restaurant response) {
                                    Log.i("Login", String.valueOf(response.getId()));
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putLong(ID_KEY, response.getId());
                                    editor.apply();
                                    fetchArticles();
                                    ArticleManager articleManager = new ArticleManager(getApplicationContext());
                                    articleManager.syncArticles();
                                    goToDashboard();
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.i("Login with google", error.getMessage());
                                }
                            });

            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            request.putIntoHeader("imei", telephonyManager.getDeviceId());
            RestroOwnerApplication.VolleyRequestQueueProvider.getInstance(getApplicationContext()).getRequestQueue().add(request);

        } else {
            // Signed out, show unauthenticated UI
            Log.i(TAG, "Login failed");
            Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_SHORT).show();
            Log.e(TAG, result.getStatus().toString());
            Log.e(TAG, result.toString());
        }
    }

    /**
     * Fetch article names from server, to be shown in suggestions
     */
    private void fetchArticles() {
        String url = configuration.getScheme() + "://" + configuration.getAuthority() +
                "/article/read/names";
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i("Login:", response.toString());
                // TODO: save in Realm ORM and then use
                realm.beginTransaction();
                for (int i = 0; i < response.length(); i++) {
                    try {
                        ArticleName articleName = new ArticleName();
                        String name = response.get(i).toString();
                        articleName.setName(name);
                        realm.copyToRealmOrUpdate(articleName);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                realm.commitTransaction();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Login", error.getMessage());
            }
        });
        RestroOwnerApplication.VolleyRequestQueueProvider.getInstance(getApplicationContext()).getRequestQueue().add(request);
    }

    /**
     * Go to home screen.
     */
    private void goToDashboard() {
        Intent dashboardActivity = new Intent(getApplicationContext(), Home.class);
        dashboardActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplicationContext().startActivity(dashboardActivity);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(mPassword);
                }
            }

            // TODO: register the new account here.
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    /**
     * Connection call back for Google Plus Login
     */
    private class LoginConnectionCallbacks implements GoogleApiClient.ConnectionCallbacks {

        @Override
        public void onConnected(@Nullable Bundle bundle) {
            Toast.makeText(getApplicationContext(), "User is connected!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onConnectionSuspended(int i) {
            Log.e("Login", "failed with code " + i);
        }
    }

    /**
     * On login connection failed listener.
     */
    private class LoginOnConnectionFailedListener implements GoogleApiClient.OnConnectionFailedListener {

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            Log.e("Login", "connection failed");
            Log.e("Login", connectionResult.toString());
        }
    }
}
