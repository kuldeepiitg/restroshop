package com.restroshop.restroowner;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.restroshop.restroowner.home.ShoppingBasket;
import com.restroshop.restroowner.storage.ArticleManager;
import com.restroshop.restroowner.storage.SessionManager;
import com.restroshop.webservices.model.Basket;

import java.util.List;

import in.co.palup.android.utilities.ServerConfiguration;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Restro Owner Application.
 * <p/>
 * Created by kuldeep on 17/07/16.
 */
public class RestroOwnerApplication extends Application {

    /**
     * Server IP and Port configuration
     */
    private ServerConfiguration configuration;

    /**
     * Article manager
     */
    private ArticleManager articleManager;

    /**
     * Session manager
     */
    private SessionManager sessionManager;

    /**
     * Location manager
     */
    private LocationManager locationManager;

    /**
     * Basket
     */
    private ShoppingBasket basket;

    @Override
    public void onCreate() {
        super.onCreate();
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder(this)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
        this.configuration = new ServerConfiguration(this);
        this.articleManager = new ArticleManager(this);
        this.sessionManager = new SessionManager(this);
        this.basket = new ShoppingBasket();
    }

    public Basket getBasket() {
        return basket;
    }

    /**
     * Get configuration set in restroshop.properties
     *
     * @return configuration
     */
    public ServerConfiguration getConfiguration() {
        return configuration;
    }

    /**
     * Get article manager.
     * <p/>
     * Note : {@link ArticleManager} should be used only from Application context.
     * Don't create new instances in activities.
     *
     * @return application {@link ArticleManager}.
     */
    public ArticleManager getArticleManager() {
        return articleManager;
    }

    /**
     * Get session manager.
     *
     * @return Session manager
     */
    public SessionManager getSessionManager() {
        return sessionManager;
    }

    /**
     * Get last known location of device
     *
     * @return
     */
    public Location getLastKnownLocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }
            Location location = locationManager.getLastKnownLocation(provider);
            if (location == null) {
                continue;
            }
            if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = location;
            }
        }
        return bestLocation;
    }

    /**
     * Singleton provider for volley request queue.
     * <p/>
     * Created by Kuldeep Yadav on 05/07/16.
     */
    public static class VolleyRequestQueueProvider {

        /**
         * Instance of VolleyRequestQueueProvider.
         */
        private static VolleyRequestQueueProvider instance;

        /**
         * Volley request queue.
         */
        private static RequestQueue requestQueue;

        /**
         * Application context
         */
        private static Context context;

        private VolleyRequestQueueProvider(Context context) {
            VolleyRequestQueueProvider.context = context;
            requestQueue = getRequestQueue();
        }

        /**
         * @param context the application context
         * @return instance of VolleyRequestQueueProvider
         */
        public static synchronized VolleyRequestQueueProvider getInstance(Context context) {

            if (VolleyRequestQueueProvider.instance == null) {
                VolleyRequestQueueProvider.instance = new VolleyRequestQueueProvider(context);
            }
            return instance;
        }

        /**
         * @return volley request queue
         * @see RequestQueue
         */
        public RequestQueue getRequestQueue() {

            if (requestQueue == null) {
                requestQueue = Volley.newRequestQueue(context);
            }

            return requestQueue;
        }
    }
}
