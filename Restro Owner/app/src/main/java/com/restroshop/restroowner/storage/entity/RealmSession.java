package com.restroshop.restroowner.storage.entity;

import com.restroshop.dao.model.Session;

import java.sql.Timestamp;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Session object to be stored into realm.
 * <p/>
 * Created by Kuldeep Yadav on 31/07/16.
 */
public class RealmSession extends RealmObject {

    /**
     * IMEI number of device with which user is logged in.
     */
    @PrimaryKey
    private String imei;

    /**
     * User id of logged in session
     */
    private long userId;

    /**
     * Session object id
     */
    private long id;

    /**
     * Creation timestamp of session.
     */
    private long createdOn;

    /**
     * Last update timestamp of session.
     */
    private long lastUpdate;

    public RealmSession() {
    }

    public RealmSession(Session session) {
        this.id = session.getId();
        this.userId = session.getUserId();
        this.imei = session.getImei();
        this.createdOn = session.getCreatedOn().getTime();
        this.lastUpdate = session.getLastUpdate().getTime();
    }

    /**
     * Convert RealmSession to {@link Session}.
     *
     * @return Session
     */
    public Session toSession() {
        Session session = new Session(imei, userId);
        session.setId(id);
        session.setLastUpdate(new Timestamp(lastUpdate));
        session.setCreatedOn(new Timestamp(createdOn));
        return session;
    }
}
