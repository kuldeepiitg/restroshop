package com.restroshop.restroowner.storage;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.restroshop.dao.model.Article;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.restroowner.storage.entity.RealmArticle;
import com.restroshop.restroowner.storage.entity.SyncTimestamp;
import com.restroshop.volley.GsonRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.co.palup.android.utilities.ServerConfiguration;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * It manages articles across server and client,
 * Checks if articles have been updated recently on server side.
 * If yes, the synchronize client side db with server side.
 * All article infos will be provided by {@link ArticleManager}.
 * An activity doesn't need to make http call to server.
 * <p/>
 * Created by kuldeep on 17/07/16.
 */
public class ArticleManager {

    /**
     * Socket timeout
     */
    private static final int MY_SOCKET_TIMEOUT_MS = 10 * 1000;

    /**
     * Realm instance for db transactions.
     */
    private Realm realm;

    /**
     * Application context
     */
    private RestroOwnerApplication appContext;

    /**
     * List of all articles
     */
    private Map<Long, Article> articles;

    /**
     * On sync completion listener
     */
    private OnSyncCompletionListener onSyncCompletionListener;

    public ArticleManager(Context context) {
        this.appContext = (RestroOwnerApplication) context;
        realm = Realm.getDefaultInstance();
    }

    /**
     * Sync articles from server if new have been added or old is updated
     * after last sync.
     */
    public void syncArticles() {

        ServerConfiguration configuration = appContext.getConfiguration();
        SyncTimestamp lastSyncTime = realm.where(SyncTimestamp.class).findFirst();

        // 0 will fetch all the articles.
        long lastSyncTimestamp = 0;
        if (lastSyncTime != null) {
            lastSyncTimestamp = lastSyncTime.getTime();
        }
        String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/" +
                "article/read/changes";
        GsonRequest<List<Article>> request = new GsonRequest<>(url, String.valueOf(lastSyncTimestamp),
                new TypeToken<List<Article>>() {
                }.getType(), new Response.Listener<List<Article>>() {
            @Override
            public void onResponse(List<Article> response) {
                SyncTimestamp presentSyncTimestamp = new SyncTimestamp(Article.class.toString());
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(RealmArticle.toRealmArticleList(response));
                realm.copyToRealmOrUpdate(presentSyncTimestamp);
                realm.commitTransaction();

                // If there are changes in articles
                // then invalidate im memory list of articles
                // articles set can be initialized here if performance is degrading
                if (response.size() > 0) {
                    articles = null;
                }
                if (onSyncCompletionListener != null) {
                    onSyncCompletionListener.onSyncCompletion();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Article Manager", error.toString());
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RestroOwnerApplication.VolleyRequestQueueProvider.getInstance(appContext).getRequestQueue().add(request);
    }

    /**
     * Read article by id.
     *
     * @param id id of article
     * @return article present in realm
     */
    public Article getById(long id) {
        // TODO: can implement cache here if needed.
        if (this.articles != null && this.articles.containsKey(id)) {
            return this.articles.get(id);
        }
        RealmArticle realmArticle = realm.where(RealmArticle.class).equalTo("id", id).findFirst();
        Article article = realmArticle.toArticle();
        if (this.articles == null) {
            this.articles = new HashMap<>();
        }
        this.articles.put(article.getId(), article);
        return article;
    }

    /**
     * Get list of all articles present in realm.
     *
     * @return list of all synced articles.
     */
    public List<Article> getAll() {
        List<Article> articleList = new ArrayList<>();
        if (this.articles != null) {
            for (Article article : this.articles.values()) {
                articleList.add(article);
            }
            return articleList;
        }
        RealmResults<RealmArticle> realmArticles = realm.where(RealmArticle.class).findAll();
        for (RealmArticle realmArticle : realmArticles) {
            Article article = realmArticle.toArticle();
            articleList.add(article);
            this.articles.put(article.getId(), article);
        }
        return articleList;
    }

    /**
     * Set OnSyncCompletionListener for article manager.
     *
     * @param onSyncCompletionListener {@link OnSyncCompletionListener}
     */
    public void setOnSyncCompletionListener(OnSyncCompletionListener onSyncCompletionListener) {
        this.onSyncCompletionListener = onSyncCompletionListener;
    }

    /**
     * On synchronization completion listener
     */
    public interface OnSyncCompletionListener {

        /**
         * Execute on synchronization completion
         */
        void onSyncCompletion();
    }
}
