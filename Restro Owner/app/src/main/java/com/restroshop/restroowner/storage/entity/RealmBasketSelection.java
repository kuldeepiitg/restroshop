package com.restroshop.restroowner.storage.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Basket selection,
 * quantity of and article present in basket.
 * <p>
 * <p>
 * Created by kuldeep on 08/08/16.
 */
public class RealmBasketSelection extends RealmObject {

    @PrimaryKey
    private long articleId;
    private int quantity;

    public RealmBasketSelection() {
    }

    public RealmBasketSelection(long articleId, int quantity) {
        this.articleId = articleId;
        this.quantity = quantity;
    }

    /**
     * Get id of article.
     *
     * @return article id
     */
    public long getArticleId() {
        return articleId;
    }

    /**
     * Set article id.
     *
     * @param articleId id of article
     */
    public void setArticleId(long articleId) {
        this.articleId = articleId;
    }

    /**
     * Quantity of article.
     *
     * @return article quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Set quantity
     *
     * @param quantity of article
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
