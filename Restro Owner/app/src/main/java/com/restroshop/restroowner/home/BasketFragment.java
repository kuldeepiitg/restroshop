package com.restroshop.restroowner.home;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.restroshop.dao.model.BasketItem;
import com.restroshop.dao.model.Order;
import com.restroshop.dao.model.SellingRate;
import com.restroshop.dao.model.Session;
import com.restroshop.restroowner.R;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.restroowner.decoration.DividerItemDecoration;
import com.restroshop.restroowner.splash.LoginFragment;
import com.restroshop.restroowner.storage.SessionManager;
import com.restroshop.webservices.model.Basket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Basket containing user's current selection with quantity.
 * <p>
 * <p>
 * Created by kuldeep on 08/08/16.
 */
public class BasketFragment extends Fragment {

    /**
     * Argument keys
     */
    public static String ORDER_KEY = "order";
    /**
     * Basket containing articles
     */
    private Basket basket;
    /**
     * UI components
     */
    private RecyclerView recyclerView;
    private TextView itemCountView;
    private TextView totalAmount;
    /**
     * Basket articles adapter
     */
    private BasketArticlesAdapter adapter;
    /**
     * Rates of article present in basket and more but not all.
     */
    private Map<Long, SellingRate> rates;

    /**
     * Session manager
     */
    private SessionManager sessionManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setHasOptionsMenu(true);
        List<SellingRate> rateList
                = (ArrayList<SellingRate>) getArguments()
                .getSerializable(ShoppingFragment.RATES_KEY);
        rates = new HashMap<>();
        for (SellingRate rate : rateList) {
            rates.put(rate.getArticle().getId(), rate);
        }
        this.basket = ((RestroOwnerApplication) getActivity().getApplicationContext()).getBasket();
        this.adapter = new BasketArticlesAdapter(rates, getActivity().getApplicationContext());
        this.adapter.setOnAnyChangeListener(new BasketArticlesAdapter.OnAnyChangeListener() {
            @Override
            public void update(int itemCount, int totalCost) {
                updateCheckoutBar(itemCount, totalCost);
            }
        });
        this.sessionManager
                = ((RestroOwnerApplication) getActivity()
                .getApplicationContext()).getSessionManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_basket, container, false);
        this.recyclerView = (RecyclerView) rootView.findViewById(R.id.basket_item_list);
        this.itemCountView = (TextView) rootView.findViewById(R.id.basket_item_count);
        this.totalAmount = (TextView) rootView.findViewById(R.id.basket_total);
        final View basketCheckout = rootView.findViewById(R.id.basket_checkout);
        basketCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLoggedInUser()) {
                    goToLogin();
                } else {
                    goToDeliveryAddressForm();
                }
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setItemAnimator(new DefaultItemAnimator());
        this.recyclerView.setAdapter(adapter);
        DividerItemDecoration divider = new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(divider);

        updateCheckoutBar(basket.getEntrySet().size(), getBasketCost());

        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_basket, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.empty_basket) {
            basket.empty();
            adapter.notifyDataSetChanged();
            updateCheckoutBar(basket.getEntrySet().size(), getBasketCost());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Go to login.
     */
    private void goToLogin() {
        Fragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putBoolean(LoginFragment.SHOW_SKIP_KEY, false);
        fragment.setArguments(args);
        // TODO: 16/08/16 It would have been better if we set this listener through LoginFragment
        sessionManager.setOnSessionRequestCompletionListener(new SessionManager.OnSessionRequestCompletionListener() {
            @Override
            public void onComplete() {
                goToDeliveryAddressForm();
            }
        });
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }

    /**
     * Go to delivery address form.
     */
    private void goToDeliveryAddressForm() {

        Fragment fragment = new DeliveryAddressFragment();
        Bundle args = new Bundle();
        args.putSerializable(ORDER_KEY, prepareOrder());
        fragment.setArguments(args);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack("delivery_address")
                .commit();
    }

    /**
     * Check if user is logged in or browsing app as anonymous.
     *
     * @return true if user is logged in, false otherwise.
     */
    private boolean isLoggedInUser() {
        Session session = sessionManager.getSession();
        return session.getUserId() > 0;
    }

    /**
     * Prepare order for the basket.
     *
     * @return order
     */
    private Order prepareOrder() {
        Order order = new Order();
        Set<BasketItem> items = new HashSet<>();
        for (Map.Entry<Long, Integer> entry : basket.getEntrySet()) {
            SellingRate rate = rates.get(entry.getKey());
            BasketItem item
                    = new BasketItem(
                    entry.getKey(), rate.getArticle().getName(),
                    rate.getArticle().getUnit().name(), rate.getPrice(),
                    basket.getQuantity(entry.getKey()));
            items.add(item);
        }
        order.setItems(items);
        return order;
    }

    /**
     * Get total price of basket.
     *
     * @return price of basket
     */
    public int getBasketCost() {
        int cost = 0;
        for (Map.Entry<Long, Integer> entry : basket.getEntrySet()) {
            cost += entry.getValue() * rates.get(entry.getKey()).getPrice();
        }
        return cost;
    }

    /**
     * Update view on changes in basket
     */
    public void updateCheckoutBar(int itemCount, int basketCost) {
        Resources resources = getResources();
        String itemCountText = String.format(resources.getString(R.string.items), itemCount);
        this.itemCountView.setText(itemCountText);

        String basketCostText = String.format(resources.getString(R.string.basket_total), basketCost);
        this.totalAmount.setText(basketCostText);
    }
}
