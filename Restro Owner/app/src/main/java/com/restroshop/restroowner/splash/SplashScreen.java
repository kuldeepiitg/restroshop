package com.restroshop.restroowner.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.restroshop.dao.model.Session;
import com.restroshop.restroowner.Home;
import com.restroshop.restroowner.R;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.restroowner.storage.ArticleManager;
import com.restroshop.restroowner.storage.SessionManager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Splash screen activity
 */
public class SplashScreen extends AppCompatActivity {

    /**
     * Article manager
     */
    private ArticleManager articleManager;

    /**
     * Session manager
     */
    private SessionManager sessionManager;

    /**
     * Count of sync requests.
     * For every newly added request to this class, request count is to be incremented.
     * <p/>
     * The count tracks how many request have to be fired.
     */
    private int REQUEST_COUNT = 1;

    /**
     * Requests to be made to server.
     * Because it is to be accessed by multiple threads, needs to be used in mutex,
     * I am using {@link AtomicInteger}. It will be set initially to number of requests to be made
     * and then response to each request will decrease the count.
     */
    private AtomicInteger requestCount;

    /**
     * Minimum show time for splash screen,
     * Splash screen must show for at least this much time
     * if data loading is fast.
     */
    private long MIN_SHOW_TIME = 1 * 1000;

    /**
     * Start time stamp of splash screen
     */
    private long splashScreenStartTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        this.articleManager = ((RestroOwnerApplication) getApplicationContext()).getArticleManager();
        this.sessionManager = ((RestroOwnerApplication) getApplicationContext()).getSessionManager();
        requestCount = new AtomicInteger(REQUEST_COUNT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        splashScreenStartTime = System.currentTimeMillis();
        loadArticleData();
    }

    /**
     * Load frequently used data
     */
    private void loadArticleData() {
        // Load changes in articles list
        this.articleManager.setOnSyncCompletionListener(new ArticleManager.OnSyncCompletionListener() {
            @Override
            public void onSyncCompletion() {
                if (requestCount.decrementAndGet() == 0) {
                    lookSession();
                    long remainingTime = MIN_SHOW_TIME - (System.currentTimeMillis() - splashScreenStartTime);
                    Log.i(SplashScreen.class.getName(), String.valueOf(remainingTime));
                    if (remainingTime > 0) {
                        try {
                            Thread.sleep(remainingTime);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        articleManager.syncArticles();
    }

    /**
     * Lookup session in client side
     */
    private void lookSession() {
        Session session = sessionManager.savedSession();
        if (session == null) {
            // launch login fragment
            Fragment fragment = prepareLoginFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.splash_screen_content_frame, fragment).commit();
        } else {
            Intent dashboardActivity = new Intent(getApplicationContext(), Home.class);
            startActivity(dashboardActivity);
        }
    }

    /**
     * Prepare login fragment and callback on successful login.
     *
     * @return fragment
     */
    private Fragment prepareLoginFragment() {
        Fragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putBoolean(LoginFragment.SHOW_SKIP_KEY, true);
        fragment.setArguments(args);
        sessionManager = ((RestroOwnerApplication) getApplicationContext()).getSessionManager();
        sessionManager.setOnSessionRequestCompletionListener(
                new SessionManager.OnSessionRequestCompletionListener() {
                    @Override
                    public void onComplete() {
                        Intent intent = new Intent(getApplicationContext(), Home.class);
                        startActivity(intent);
                    }
                }
        );
        return fragment;
    }
}
