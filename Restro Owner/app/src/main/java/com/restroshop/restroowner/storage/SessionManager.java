package com.restroshop.restroowner.storage;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.restroshop.dao.model.Restaurant;
import com.restroshop.dao.model.Session;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.restroowner.storage.entity.RealmSession;
import com.restroshop.volley.GsonRequest;

import in.co.palup.android.utilities.ServerConfiguration;
import io.realm.Realm;

/**
 * Session manager, All utilities related to user session.
 * <p/>
 * Created by kuldeep on 31/07/16.
 */
public class SessionManager {

    /**
     * Realm instance for db transactions.
     */
    private Realm realm;

    /**
     * Application context
     */
    private RestroOwnerApplication appContext;

    /**
     * Server configuration
     */
    private ServerConfiguration configuration;

    /**
     * Opened session
     */
    private Session session;

    /**
     * Google json serializer
     */
    private Gson gson;

    /**
     * On session request complete listener
     */
    private OnSessionRequestCompletionListener onSessionRequestCompletionListener;

    public SessionManager(Context context) {
        this.appContext = (RestroOwnerApplication) context;
        realm = Realm.getDefaultInstance();
        gson = new Gson();
    }

    /**
     * Get saved session from storage if any.
     */
    public Session savedSession() {
        configuration = appContext.getConfiguration();
        TelephonyManager telephonyManager = (TelephonyManager) appContext
                .getSystemService(Context.TELEPHONY_SERVICE);
        final String imei = telephonyManager.getDeviceId();
        RealmSession realmSession = realm.where(RealmSession.class).equalTo("imei", imei).findFirst();
        this.session = realmSession != null ? realmSession.toSession() : null;
        return session;
    }

    /**
     * Open session for logged in user.
     *
     * @param restaurant restaurant for which session is to be opened.
     */
    public void openSession(Restaurant restaurant) {
        String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/" +
                "restaurant/session/open";
        TelephonyManager telephonyManager = (TelephonyManager) appContext
                .getSystemService(Context.TELEPHONY_SERVICE);
        final String imei = telephonyManager.getDeviceId();
        GsonRequest<Session> request = new GsonRequest<Session>(url
                , gson.toJson(restaurant)
                , new TypeToken<Session>() {
        }.getType()
                , new Response.Listener<Session>() {
            @Override
            public void onResponse(Session response) {
                session = response;
                session.setImei(imei);
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(new RealmSession(session));
                realm.commitTransaction();
                onSessionRequestCompletionListener.onComplete();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(SessionManager.class.getName(), error.toString());
            }
        });
        request.putIntoHeader("imei", imei);
        RestroOwnerApplication.VolleyRequestQueueProvider
                .getInstance(appContext)
                .getRequestQueue()
                .add(request);
    }

    /**
     * Open anonymous session, session for logged out user.
     */
    public void openSession() {
        configuration = appContext.getConfiguration();
        TelephonyManager telephonyManager = (TelephonyManager) appContext
                .getSystemService(Context.TELEPHONY_SERVICE);
        final String imei = telephonyManager.getDeviceId();
        String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/" +
                "restaurant/session/open/anonymous";
        GsonRequest<Session> request = new GsonRequest<Session>(url
                , new TypeToken<Session>() {
        }.getType()
                , new Response.Listener<Session>() {
            @Override
            public void onResponse(Session response) {
                session = response;
                session.setImei(imei);
                realm.beginTransaction();
                realm.copyToRealm(new RealmSession(session));
                realm.commitTransaction();
                onSessionRequestCompletionListener.onComplete();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(SessionManager.class.getName(), error.toString());
            }
        });
        request.putIntoHeader("imei", imei);
        RestroOwnerApplication.VolleyRequestQueueProvider
                .getInstance(appContext)
                .getRequestQueue()
                .add(request);
    }

    /**
     * Get current session
     *
     * @return currently open session
     */
    public Session getSession() {
        return this.session;
    }

    /**
     * Set OnSessionRequestCompletionListener.
     *
     * @param onSessionRequestCompletionListener listener to take action after session is loaded
     *                                           successfully
     */
    public void setOnSessionRequestCompletionListener(
            OnSessionRequestCompletionListener onSessionRequestCompletionListener) {

        this.onSessionRequestCompletionListener = onSessionRequestCompletionListener;
    }

    /**
     * On session request complete listener.
     */
    public interface OnSessionRequestCompletionListener {
        void onComplete();
    }
}
