package com.restroshop.restroowner.splash;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.restroshop.dao.model.Restaurant;
import com.restroshop.restroowner.R;
import com.restroshop.restroowner.RestroOwnerApplication;
import com.restroshop.restroowner.storage.SessionManager;

import in.co.palup.android.utilities.ServerConfiguration;

/**
 * Login fragment
 * <p/>
 * Created by Kuldeep Yadav on 31/07/16.
 */
public class LoginFragment extends Fragment {

    /**
     * Signin request code to identify response
     */
    private static final int RC_SIGN_IN = 9001;
    /**
     * Show skip button key
     */
    public static String SHOW_SKIP_KEY = "show_skip";
    /**
     * Google login button
     */
    private SignInButton loginButton;
    /**
     * Skip label
     */
    private TextView skipLabel;
    /**
     * Google API Client
     */
    private GoogleApiClient mGoogleApiClient;
    /**
     * Google API client connection callback
     */
    private GoogleApiClient.ConnectionCallbacks mConnectionCallbacks;
    /**
     * Google API client connection failed listener
     */
    private GoogleApiClient.OnConnectionFailedListener mConnectionFailedListener;
    /**
     * Server configuration
     */
    private ServerConfiguration configuration;
    /**
     * Session manager
     */
    private SessionManager sessionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.sessionManager = ((RestroOwnerApplication) getActivity().getApplicationContext()).getSessionManager();

        View fragmentView = inflater.inflate(R.layout.fragment_login, container, false);
        skipLabel = (TextView) fragmentView.findViewById(R.id.fragment_login_skip);
        loginButton = (SignInButton) fragmentView.findViewById(R.id.fragment_login_google);

        Bundle args = getArguments();
        if (args != null) {
            boolean skip = getArguments().getBoolean(SHOW_SKIP_KEY);
            if (!skip) {
                skipLabel.setVisibility(View.INVISIBLE);
            }
        }

        initGoogleLogin(fragmentView);
        initSkip(fragmentView);
        return fragmentView;
    }

    /**
     * Initialize skip login label action
     */
    private void initSkip(View fragmentView) {
        skipLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.openSession();
            }
        });
    }

    /**
     * Login with google action
     */
    private void initGoogleLogin(View fragmentView) {

        // Initializing google plus api client
        mConnectionCallbacks = new LoginConnectionCallbacks();
        mConnectionFailedListener = new LoginOnConnectionFailedListener();
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(mConnectionCallbacks)
                .addOnConnectionFailedListener(mConnectionFailedListener)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount account = result.getSignInAccount();
                assert account != null;
                Restaurant restaurant = new Restaurant();
                Location location = ((RestroOwnerApplication) getActivity().getApplicationContext())
                        .getLastKnownLocation();
                restaurant.setLocation(
                        new com.restroshop.dao.model.Location(
                                location.getLatitude(), location.getLongitude()));
                restaurant.setEmail(account.getEmail());
                // A callback is made by session manager
                sessionManager.openSession(restaurant);
            } else {
                // Signed out, show unauthenticated UI
                Log.i(LoginFragment.class.getName(), "Login failed");
                Toast.makeText(getActivity().getApplicationContext(), "Login failed", Toast.LENGTH_SHORT).show();
                Log.e(LoginFragment.class.getName(), result.getStatus().toString());
                Log.e(LoginFragment.class.getName(), result.toString());
            }
        }
    }

    /**
     * Connection call back for Google Plus Login
     */
    private class LoginConnectionCallbacks implements GoogleApiClient.ConnectionCallbacks {

        @Override
        public void onConnected(@Nullable Bundle bundle) {
            Toast.makeText(getActivity().getApplicationContext(), "User is connected!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onConnectionSuspended(int i) {
            Log.e("Login", "failed with code " + i);
        }
    }

    /**
     * On login connection failed listener.
     */
    private class LoginOnConnectionFailedListener implements GoogleApiClient.OnConnectionFailedListener {

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            Log.e("Login", "connection failed");
            Log.e("Login", connectionResult.toString());
        }
    }
}
