package com.restroshop.webservices.model;

import com.restroshop.dao.model.Location;

/**
 * Filter
 * <p>
 * Created by Kuldeep Yadav on 02/08/16.
 */
public class Filter {

    /**
     * Filter string for name of article
     */
    private String name;

    /**
     * Device location
     */
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Filter filter = (Filter) o;

        if (name != null ? !name.equals(filter.name) : filter.name != null) return false;
        return location != null ? location.equals(filter.location) : filter.location == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
