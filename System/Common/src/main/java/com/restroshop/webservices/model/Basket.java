package com.restroshop.webservices.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Basket containing items selection.
 * Each item is present in basket with non zero positive quantity
 * <p>
 * Created by Kuldeep Yadav on 07/08/16.
 */
public class Basket {

    /**
     * Selection of objects with their quantity
     */
    private Map<Long, Integer> selection;

    public Basket() {
        this.selection = new HashMap<Long, Integer>();
    }

    /**
     * Put article in basket with given quantity.
     *
     * @param articleId id of article in selection
     * @param quantity  quantity of article
     */
    public void add(long articleId, int quantity) {

        if (quantity != 0) {
            selection.put(articleId, quantity);
        } else {
            if (selection.containsKey(articleId)) {
                selection.remove(articleId);
            }
        }
    }

    /**
     * Get quantity of article.
     *
     * @param articleId id of article
     * @return quantity of article
     */
    public int getQuantity(long articleId) {

        if (!selection.containsKey(articleId)) {
            return 0;
        }
        return selection.get(articleId);
    }

    /**
     * Increment quantity by one Unit for given article.
     *
     * @param articleId id of article
     */
    public void increase(long articleId) {
        if (selection.containsKey(articleId)) {
            selection.put(articleId, 1);
        } else {
            selection.put(articleId, selection.get(articleId) + 1);
        }
    }

    /**
     * Decrease quantity of article by one unit.
     *
     * @param articleId id of article
     */
    public void decrease(long articleId) {
        if (selection.containsKey(articleId)) {
            selection.put(articleId, selection.get(articleId) - 1);
        }

        if (selection.get(articleId) == 0) {
            selection.remove(articleId);
        }
    }

    /**
     * Remove article from basket.
     *
     * @param articleId id of article to be removed
     */
    public void remove(long articleId) {
        selection.remove(articleId);
    }

    public Set<Map.Entry<Long, Integer>> getEntrySet() {
        return selection.entrySet();
    }

    /**
     * Empty the basket.
     */
    public void empty() {
        selection.clear();
    }
}
