package com.restroshop.dao.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A {@link Person} who is responsible for execution
 * of a restaurant.
 * <p/>
 * Created by Kuldeep Yadav on 28-Jun-16.
 *
 * @see Person
 */
public class RestroManager extends User implements Serializable {

    /**
     * Restaurants managed by the manager.
     */
    private Set<Restaurant> restaurants = new HashSet<Restaurant>();

    public Set<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(Set<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    /**
     * Add a restaurant to be managed by the manager.
     *
     * @param restaurant restaurant to be added.
     */
    public void addRestaurant(Restaurant restaurant) {
        this.restaurants.add(restaurant);
    }
}
