package com.restroshop.dao.model.transiant;

/**
 * Postal address of a location.
 * <p>
 * Created by Kuldeep Yadav on 11/08/16.
 */
public class Address {

    /**
     * Complex/building name
     */
    private String complexName;

    /**
     * Street name
     */
    private String street;

    /**
     * Area name
     */
    private String area;

    /**
     * City name
     */
    private String city;

    /**
     * State name
     */
    private String state;

    /**
     * Postal Index Code of area
     */
    private String pincode;

    public Address(String complexName, String street, String area, String city, String state, String pincode) {
        this.complexName = complexName;
        this.street = street;
        this.area = area;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
    }

    public Address() {

    }

    public String getComplexName() {
        return complexName;
    }

    public void setComplexName(String complexName) {
        this.complexName = complexName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }
}
