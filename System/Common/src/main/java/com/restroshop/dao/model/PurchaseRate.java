package com.restroshop.dao.model;

import java.io.Serializable;

/**
 * Purchase rate of an article.
 * <p>
 * Created by Kuldeep Yadav on 11-Jul-16.
 */
public class PurchaseRate extends BaseEntity implements Serializable {

    /**
     * Article for which purchase is made
     */
    private Article article;

    /**
     * Availability(quantity) of article in shop.
     */
    private int availability;

    /**
     * Price at purchase is made
     */
    private int price;

    /**
     * Profit margin to be earned per unit of article
     */
    private int profitMargin;

    public long getId() {
        return super.id;
    }

    /**
     * Get article for which purchase is made.
     *
     * @return associated article
     */
    public Article getArticle() {
        return article;
    }

    /**
     * Set article for which purchase is to be made.
     *
     * @param article associated article.
     */
    public void setArticle(Article article) {
        this.article = article;
    }

    /**
     * Get per unit purchasing price of article.
     *
     * @return price for per unit purchase of article.
     */
    public int getPrice() {
        return price;
    }

    /**
     * Set price for per unit purchase of article
     *
     * @param price for per unit purchase
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * Get profit margin
     *
     * @return profit margin on sale of unit article.
     */
    public int getProfitMargin() {
        return profitMargin;
    }

    /**
     * Set profit margin to be earned on unit article.
     *
     * @param profitMargin profit margin on unit article sale.
     */
    public void setProfitMargin(int profitMargin) {
        this.profitMargin = profitMargin;
    }

    /**
     * Get quantity of article present in shop. A negative quantity tells, orders have been placed
     * and there is a deficit of article.
     *
     * @return available quantity of article.
     */
    public int getAvailability() {
        return availability;
    }

    /**
     * Set quantity of article present in shop.
     *
     * @param availability available quantity of article.
     */
    public void setAvailability(int availability) {
        this.availability = availability;
    }

    /**
     * Add quantity to availability. Some more quantity of article have been purchased
     * and is added to availability.
     *
     * @param quantity quantity of articles to be added in shop.
     */
    public void addAvailability(int quantity) {
        this.availability += quantity;
    }
}
