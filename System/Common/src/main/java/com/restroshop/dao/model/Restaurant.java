package com.restroshop.dao.model;

import java.io.Serializable;

/**
 * Restaurant
 * <p/>
 * Created by Kuldeep Yadav on 27-Jun-16.
 */
public class Restaurant extends SalePoint implements Serializable {

    /**
     * Url for pic
     */
    private String picUrl;

    /**
     * Email id of restaurant
     */
    private String email;

    /**
     * Fixed phone of restaurant
     */
    private String phone;

    public Restaurant(String name, double latitude, double longitude, String picUrl,
                      String restroEmail, String restroPhone) {
        super(name, latitude, longitude);
        this.picUrl = picUrl;
        this.email = restroEmail;
        this.phone = restroPhone;
    }

    public Restaurant() {
        super();
        // For hibernate
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
