package com.restroshop.dao.model;

import java.io.Serializable;

/**
 * Article(vegetable/fruit/goods).
 * <p/>
 * Created by Kuldeep Yadav on 20-Jun-16.
 */
public class Article extends BaseEntity implements Serializable {

    /**
     * Name of article
     */
    private String name;

    /**
     * Local url of pic for article.
     */
    private String picUrl;

    /**
     * Description of article.
     */
    private String description;

    /**
     * Unit of measurement of article;
     */
    private Unit unit;

    /**
     * Get article id.
     *
     * @return id of the article.
     */
    public long getId() {
        return super.id;
    }

    /**
     * Get name of the article.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name of the article.
     *
     * @param name name of the article
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get local url for the pic of article.
     *
     * @return the url suffix for pic.
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * Set url suffix for pic.
     *
     * @param picUrl the url suffix for pic.
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * Get article's detailed description.
     *
     * @return article description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set article's detailed description.
     *
     * @param description description to be set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get unit of the rate.
     *
     * @return the unit.
     */
    public Unit getUnit() {
        return unit;
    }

    /**
     * Set unit of the rate.
     *
     * @param unit the unit to be set.
     */
    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Article{" +
                "name='" + name + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", description='" + description + '\'' +
                ", unit=" + unit +
                '}';
    }

}
