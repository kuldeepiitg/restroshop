package com.restroshop.dao.model;

/**
 * Session for logged in user
 * <p>
 * Created by Kuldeep Yadav on 10/07/16.
 */
public class Session extends BaseEntity {

    /**
     * IMEI of device, user is using
     */
    private String imei;

    /**
     * Id of user
     */
    private long userId;

    public Session(String imei, long id) {
        this.imei = imei;
        this.userId = id;
    }

    public Session() {
        // For hibernate
    }

    /**
     * Get IMEI number of device
     *
     * @return IMEI number
     */
    public String getImei() {
        return imei;
    }

    /**
     * Set IMEI of device into session.
     *
     * @param imei
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     * Get user/restaurant id.
     *
     * @return user id or restaurant id
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Set user id or restaurant id
     *
     * @param userId
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }
}
