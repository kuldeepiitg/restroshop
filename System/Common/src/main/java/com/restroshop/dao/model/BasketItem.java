package com.restroshop.dao.model;

import java.io.Serializable;

/**
 * An article in order with quantity, unit name, unit price.
 */
public class BasketItem extends BaseEntity implements Serializable {

    /**
     * Id of article(item)
     */
    private long articleId;

    /**
     * Name of article(item)
     */
    private String articleName;

    /**
     * Unit with which articles are measured
     */
    private String unit;

    /**
     * Price of unit article
     */
    private int rate;

    /**
     * Quantity of article present in basket
     */
    private int quantity;

    public BasketItem(long articleId, String articleName, String unit, int rate, int quantity) {
        this.articleId = articleId;
        this.articleName = articleName;
        this.unit = unit;
        this.rate = rate;
        this.quantity = quantity;
    }

    public BasketItem() {
        // For hibernate
    }

    /**
     * Get article id of item.
     *
     * @return id of article
     */
    public long getArticleId() {
        return articleId;
    }

    public void setArticleId(long articleId) {
        this.articleId = articleId;
    }

    /**
     * Get article name of item.
     *
     * @return name of article
     */
    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    /**
     * Get unit of measurement of article.
     *
     * @return unit of measurement
     */
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Get price of unit quantity of article.
     *
     * @return unit price
     */
    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    /**
     * Get quantity of article added into basket.
     *
     * @return quantity of article
     */
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
