package com.restroshop.dao.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A person, can be a restaurant owner, farmer, driver, system administrator.
 * <p/>
 * Created by Kuldeep Yadav on 27-Jun-16.
 */
public class Person extends BaseEntity {

    /**
     * Name of the person
     */
    private String name;

    /**
     * Email id of person
     */
    private String email;

    /**
     * Phone number of person
     */
    private String phone;

    /**
     * Local url of person's pic
     */
    private String picUrl;

    /**
     * Metadata tags for person.
     *
     * Let's say there are multiple point of contacts
     * in a restaurant. Each of them have a post/level
     * associated. We can store such information in tags.
     */
    public Set<String> tags = new HashSet<String>();

    public Person() {
        // For Hibernate
    }

    public long getId() {
        return super.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * Get metadata tags for a person.
     *
     * @return tags
     */
    public Set<String> getTags() {
        return tags;
    }

    /**
     * Remove existing tags and add given tags.
     *
     * @param tags tags to be added
     *
     * @deprecated
     * Use {@link Person#addTag(Collection)} to add tags.
     * Because addTag does't remove existing tags but merge
     * new into existing.
     */
    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    /**
     * Add a new tag to the person.
     *
     * @param tag tag to be added.
     */
    public void addTag(String tag) {
        this.tags.add(tag);
    }

    /**
     * Add new tags to the person.
     *
     * @param tags collection of tags
     */
    public void addTag(Collection<String> tags) {
        this.tags.addAll(tags);
    }

}
