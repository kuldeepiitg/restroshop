package com.restroshop.dao.model;

import java.io.Serializable;
import java.util.Set;

/**
 * Order placed by a restaurant
 * is defined by Delivery Address, set of items with quantity,
 * price set of ordered items.
 * <p>
 * Created by Kuldeep Yadav on 12/08/16.
 */
public class Order extends BaseEntity implements Serializable {

    /**
     * Delivery address
     */
    private String contactPerson;
    private String phone;
    private String restaurantName;
    private String complex;
    private String street;
    private String area;
    private String city;
    private String state;
    private String pincode;

    /**
     * The basket to be delivered
     */
    private Set<BasketItem> items;

    /**
     * Get name of contact person that will be contacted while delivering the order.
     *
     * @return name of person whom order will be handed over.
     */
    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * Get phone number of contact person.
     *
     * @return phone number of contact person.
     */
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Get name of restaurant for which order is made.
     *
     * @return name of restaurant
     */
    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    /**
     * Get name of street in address.
     *
     * @return name of street
     */
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Get name of area in the city
     *
     * @return area name
     */
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    /**
     * Get city name in address.
     *
     * @return city name
     */
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Get state name in address.
     *
     * @return state name
     */
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    /**
     * Get Postal Index Number code of address.
     *
     * @return pin code of delivery point
     */
    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     * Get building/complex name where restaurant is located.
     *
     * @return name of building.
     */
    public String getComplex() {
        return complex;
    }

    public void setComplex(String complex) {
        this.complex = complex;
    }

    /**
     * Read invoice id for order.
     *
     * @return invoice id
     */
    public String readInvoiceId() {

        return "inv-" + getId();
    }

    public Set<BasketItem> getItems() {
        return items;
    }

    public void setItems(Set<BasketItem> articles) {
        this.items = articles;
    }

    /**
     * Read item count.
     *
     * @return count of items
     */
    public int readItemCount() {
        return items.size();
    }

    /**
     * Read basket cost.
     *
     * @return total cost of basket
     */
    public int readBasketCost() {
        int cost = 0;
        for (BasketItem item : items) {
            cost += item.getQuantity() * item.getRate();
        }
        return cost;
    }
}
