package com.restroshop.dao.model;

import com.restroshop.dao.model.transiant.Address;

import java.io.Serializable;

/**
 * A sale point is a place to where we sell our articles. A sale point
 * can be a geographical point or geographical area.
 * <p>
 * For example, Bangalore city as a whole is a sell point.
 * A restaurant in Bangalore is another example of sell point.
 * A sale point differs from another sell point by the articles price.
 * Potato is sold to Mast Kalandar restaurant at rate of Rs 20/KG compared to
 * Punjabi Rasoi at rate of Rs 25/Kg. And for bangalore city(area level)
 * is Rs 29/Kg where prices are not negotiated.
 * <p>
 * Created by Kuldeep Yadav on 22-Jun-16.
 */
public class SalePoint extends BaseEntity implements Serializable {

    /**
     * Name of the restaurant/city.
     */
    private String name;

    /**
     * Latitude of location, where sale point is situated.
     */
    private double latitude;

    /**
     * Longitude of location, where sale point is situated.
     */
    private double longitude;

    /**
     * Complex/building name
     */
    private String complexName;

    /**
     * Street name
     */
    private String street;

    /**
     * Area name
     */
    private String area;

    /**
     * City name
     */
    private String city;

    /**
     * State name
     */
    private String state;

    /**
     * Postal Index Code of area
     */
    private String pincode;

    /**
     * Construct a sale point
     *
     * @param name      name of restaurant
     * @param latitude  latitude of location
     * @param longitude longitude of location
     */
    public SalePoint(String name, double latitude, double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public SalePoint() {
        // for hibernate
    }

    /**
     * Get sale point id.
     *
     * @return id for the sale point.
     */
    public long getId() {
        return super.id;
    }

    /**
     * @return name of sale point.
     */
    public String getName() {
        return name;
    }

    /**
     * Set name of sale point.
     *
     * @param name name of the sale point.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return latitude of sale point
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Set latitude of sale point.
     *
     * @param latitude latitude of sale point
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return longitude of sale point.
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Set longitude of sale point
     *
     * @param longitude longitude of sale point
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * Get location of warehouse.
     *
     * @return location
     */
    public Location readLocation() {
        if (getLatitude() == 0 && getLongitude() == 0) {
            return null;
        }
        return new Location(getLatitude(), getLongitude());
    }

    /**
     * Set location of warehouse.
     *
     * @param location location to be set.
     */
    public void setLocation(Location location) {
        setLatitude(location.getLatitude());
        setLongitude(location.getLongitude());
    }

    /**
     * Get complex name in address.
     *
     * @return complex/building name
     */
    public String getComplexName() {
        return complexName;
    }

    public void setComplexName(String complexName) {
        this.complexName = complexName;
    }

    /**
     * Get street/road name where sale point is located.
     *
     * @return road name
     */
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Get area name.
     *
     * @return area name
     */
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    /**
     * Get city in address.
     *
     * @return city name
     */
    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Get state in address.
     *
     * @return state name
     */
    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    /**
     * Get PIN(Postal Index Number) code in address.
     *
     * @return pin code
     */
    public String getPincode() {
        return this.pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     * Address of sale point.
     *
     * @return address where sale point is located.
     */
    public Address readAddress() {
        return new Address(complexName, street, area, city, state, pincode);
    }

    /**
     * Set address of sale point
     *
     * @param address address of salepoint
     */
    public void setAddress(Address address) {
        this.complexName = address.getComplexName();
        this.street = address.getStreet();
        this.area = address.getArea();
        this.city = address.getCity();
        this.state = address.getState();
        this.pincode = address.getPincode();
    }
}
