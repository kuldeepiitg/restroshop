package com.restroshop.dao.model;

import java.io.Serializable;

/**
 * User is a person who have login credentials with some role.
 * <p>
 * Created by Kuldeep Yadav on 30-Jun-16.
 */
public class User extends Person implements Serializable {

    /**
     * User password (encoded with md-5)
     */
    private String password;

    /**
     * Role of user
     */
    private Role role;

    /**
     * Remove password for the user.
     * <p>
     * Remove password from user object. When a user object is passed
     * from client to server, Jackson deserializer set value of password
     * to empty string. It should be kept {@code null}.
     */
    public void removePassword() {
        this.password = null;
    }

    /**
     * Set role for the user.
     *
     * @return role of the user.
     */
    public Role getRole() {
        return role;
    }

    /**
     * Set role of the user.
     *
     * @param role to be assigned
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Get user password checksum.
     *
     * @return checksum of user password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set user password.
     *
     * @param password password checksum to be set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Type of roles.
     */
    public enum Role {

        /* System administrator */
        ADMINISTRATOR,

        /* Restaurant Manager */
        RESTRO_MANAGER,

        /* Delivery Boy */
        DELIVERY_BOY,

        /* Supplier (farmer/store owner in mandi) */
        SUPPLIER
    }
}
