package com.restroshop.dao.model;

/**
 * Units of measurement.
 */
public enum Unit {

    // Weight
    KG,
    GRAM,
    QUINTAL,

    // Volume
    LITER,

    // Length
    METER,

    // Count
    DOZEN,
}
