package com.restroshop.dao.model;

/**
 * Article for a sale point.
 * <p/>
 * <b>Note</b>: This class is used to be client server communication.
 * <p>
 * Created by Kuldeep Yadav on 09/07/16.
 */
public class SalePointArticle extends Article {

    /**
     * Negotiated sellingRate of article for given sale point.
     */
    private int rate;

    public SalePointArticle(Article article, int rate) {
        this.id = article.getId();
        this.setName(article.getName());
        this.setPicUrl(article.getPicUrl());
        this.setDescription(article.getPicUrl());
        this.setUnit(article.getUnit());
        this.rate = rate;
    }

    /**
     * Get article's negotiated sellingRate
     *
     * @return negotiated sellingRate of article.
     */
    public int getRate() {
        return this.rate;
    }

    /**
     * Set selling rate for the article, it might be negotiated or general
     * that is available for whole city.
     *
     * @param rate negotiated selling rate
     */
    public void setRate(int rate) {
        this.rate = rate;
    }
}
