package com.restroshop.dao.model;

import java.io.Serializable;
import java.sql.Date;

/**
 * SellingRate of an article for a restaurant, or rate for the bigger area like city.
 * <p>
 * Created by Kuldeep Yadav on 23-Jun-16.
 */
public class SellingRate implements Serializable {

    /**
     * Article associated with rate
     */
    private Article article;

    /**
     * Sale point associated with rate
     */
    private SalePoint salePoint;

    /**
     * Price for the article and sale point pair.
     */
    private int price;

    /**
     * Start - End date of the period for which the sale rate is decided.
     */
    private Date startDate;
    private Date endDate;

    public SellingRate() {
        // For Hibernate
    }

    public SellingRate(Article article, SalePoint salePoint, int price, Date startDate, Date endDate) {
        this.article = article;
        this.salePoint = salePoint;
        this.price = price;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * Get associated article.
     *
     * @return article from id.
     */
    public Article getArticle() {
        return this.article;
    }

    /**
     * Set associated article into id.
     *
     * @param article article to be set.
     */
    public void setArticle(Article article) {
        this.article = article;
    }

    /**
     * Get associated sale point.
     *
     * @return sale point from id.
     */
    public SalePoint getSalePoint() {
        return this.salePoint;
    }

    /**
     * Set sale point into id.
     *
     * @param salePoint sale point to be set.
     */
    public void setSalePoint(SalePoint salePoint) {
        this.salePoint = salePoint;
    }

    /**
     * Get price of the article.
     *
     * @return price of the article.
     */
    public int getPrice() {
        return price;
    }

    /**
     * Set price for the article.
     *
     * @param price price of the article.
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * Get start date of duration for which selling rate is decided.
     *
     * @return start date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Set start date of duration.
     *
     * @param startDate start date
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Get end date of duration for which selling rate is decided.
     *
     * @return end date
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Set end date of duration
     *
     * @param endDate end date.
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean equals(Object o) {
        // TODO : analyze and rectify if needed
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SellingRate that = (SellingRate) o;
        if (article != null ? !article.equals(that.getArticle()) : that.getArticle() != null) {
            return false;
        }
        return salePoint != null ? salePoint.equals(that.getSalePoint()) : that.getSalePoint() == null;
    }

    public int hashCode() {
        // TODO : analyze and rectify if needed
        int result;
        result = (article != null ? article.hashCode() : 0);
        result = 31 * result + (salePoint != null ? salePoint.hashCode() : 0);
        return result;
    }
}
