package com.restroshop.dao.model;

import java.io.Serializable;

/**
 * Warehouse, is a point of sale for non-registered restaurants.
 * If a restaurant has not negotiated prices for articles, he will be shown
 * warehouse prices.
 * <p>
 * Created by Kuldeep Yadav on 21/07/16.
 */
public class Warehouse extends SalePoint implements Serializable {

    /**
     * Radius of area, warehouse covers.
     */
    private int radius;

    /**
     * Get radius of coverage of warehouse. All restaurants falling in coverage
     * will be supplied from this point.
     *
     * @return radius of coverage of warehouse.
     */
    public int getRadius() {
        return radius;
    }

    /**
     * Set radius of warehouse coverage.
     *
     * @param radius of warehouse coverage.
     */
    public void setRadius(int radius) {
        this.radius = radius;
    }
}
