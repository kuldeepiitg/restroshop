package com.restroshop.webservices;

import com.restroshop.dao.ArticleDaoImpl;
import com.restroshop.dao.EntityDao;
import com.restroshop.dao.PurchaseRateDaoImpl;
import com.restroshop.dao.SaleRateDaoImpl;
import com.restroshop.dao.model.Article;
import com.restroshop.dao.model.PurchaseRate;
import com.restroshop.dao.model.SalePointArticle;
import com.restroshop.dao.model.SellingRate;
import com.restroshop.session.SessionCache;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Restroshop CRUD services for {@link Article}.
 * <p/>
 * Created by Kuldeep on 19/06/16.
 */
@Path("/article")
public class ArticleService {

    private EntityDao<Article> articleDao = new ArticleDaoImpl();
    private EntityDao<SellingRate> sellingRateDao = new SaleRateDaoImpl();
    private EntityDao<PurchaseRate> purchaseRateEntityDao = new PurchaseRateDaoImpl();

    @GET
    public Response greet() {
        return Response.ok("Welcome to restroshop APIs...").build();
    }

    @GET
    @Path("/read/{id}")
    @Produces("application/json")
    public Response readArticle(@PathParam("id") final long id) {

        Article article = articleDao.read(id);
        return article != null ?
                Response.ok(article, MediaType.APPLICATION_JSON).build() :
                Response.noContent().build();
    }

    @POST
    @Path("/read/changes")
    @Consumes("application/json")
    @Produces("application/json")
    public List<Article> list(long timestamp) {

        return ((ArticleDaoImpl) articleDao).list(new Timestamp(timestamp));
    }

    @GET
    @Path("/read")
    @Produces("application/json")
    public List<Article> list() {
        return articleDao.list();
    }

    @GET
    @Path("/read/available/{startIndex}/{count}")
    @Produces("application/json")
    public List<SalePointArticle> list(@HeaderParam("userid") final long userId,
                                       @HeaderParam("imei") final String imei,
                                       @PathParam("startIndex") final int startIndex,
                                       @PathParam("count") final int count) throws ExecutionException {

        if (!SessionCache.getInstance().loggedIn(imei, userId)) {
            throw new RuntimeException("Unauthorized access. Session doesn't exist.");
        }

        List<SalePointArticle> salePointArticles = new ArrayList<SalePointArticle>();
        List<PurchaseRate> purchaseRates = ((PurchaseRateDaoImpl) purchaseRateEntityDao).list(startIndex, count,
                new Date(System.currentTimeMillis()));
        for (PurchaseRate purchaseRate : purchaseRates) {

            // userId is synonymous to salepoint_id because
            // presently, every restaurant is a user in itself
            SellingRate sellingRate = ((SaleRateDaoImpl) sellingRateDao).read(purchaseRate.getArticle().getId(),
                    userId, new Date(System.currentTimeMillis()));
            int rate = sellingRate != null ? sellingRate.getPrice() : (purchaseRate.getPrice() + purchaseRate.getProfitMargin());
            Article article = articleDao.read(purchaseRate.getArticle().getId());
            salePointArticles.add(new SalePointArticle(article, rate));
        }
        return salePointArticles;
    }

    @POST
    @Path("/update")
    public Response update(Article article) {
        articleDao.update(article);
        return Response.ok("Article updated successfully").build();
    }

    @POST
    @Path("/create")
    public long create(Article article) {
        return ((Long) articleDao.create(article));
    }

    @DELETE
    @Path("/delete/{id}")
    public Response delete(@PathParam("id") final long id) {
        articleDao.delete(id);
        return Response.ok("Article Deleted successfully").build();
    }

    @GET
    @Path("/read/names")
    @Produces("application/json")
    public List<String> readArticleNames() {
        return ((ArticleDaoImpl) articleDao).names();
    }
}
