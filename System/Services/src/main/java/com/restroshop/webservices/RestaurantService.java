package com.restroshop.webservices;

import com.restroshop.dao.*;
import com.restroshop.dao.model.*;
import com.restroshop.session.SessionCache;
import com.restroshop.webservices.cache.ArticleCache;
import com.restroshop.webservices.model.Filter;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Date;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CRUD webservices for {@link com.restroshop.dao.model.Restaurant},
 * {@link com.restroshop.dao.model.SellingRate}s for the restaurant.
 *
 * <p>
 * Created by Kuldeep Yadav on 10/07/16.
 */
@Path("/restaurant/")
public class RestaurantService {

    private EntityDao<Restaurant> restaurantDao = new RestaurantDaoImpl();
    private EntityDao<Warehouse> warehouseDao = new WarehouseDaoImpl();
    private EntityDao<SellingRate> sellingRateDao = new SaleRateDaoImpl();
    private OrderDaoImpl orderDao = new OrderDaoImpl();
    private Logger logger = Logger.getLogger(RestaurantService.class.getName());

    /**
     * Get list of ids of articles, those have names starting with namePrefix
     *
     * @param namePrefix prefix present in name
     * @return list of ids of filtered articles
     */
    private List<Long> getArticleIds(String namePrefix) throws ExecutionException {

        List<Article> articles = ArticleCache.getInstance().get();
        List<Long> ids = new ArrayList<Long>();
        for (Article article : articles) {
            if (article.getName().toLowerCase().startsWith(namePrefix.toLowerCase())) {
                ids.add(article.getId());
            }
        }
        return ids;
    }

    @POST
    @Path("session/open")
    @Produces("application/json")
    @Consumes("application/json")
    public Session openSession(@HeaderParam("imei") final String imei,
                               Restaurant restaurant) throws ExecutionException {
        Restaurant restaurantInDB = ((RestaurantDaoImpl) restaurantDao).readByEmail(restaurant.getEmail());
        if (restaurantInDB == null) {
            restaurantDao.create(restaurant);
            restaurantInDB = restaurant;
        }
        Session session = new Session(imei, restaurantInDB.getId());
        SessionCache.getInstance().put(imei, session);
        return session;
    }

    @GET
    @Path("session/open/anonymous")
    @Consumes("application/json")
    @Produces("application/json")
    public Session openSession(@HeaderParam("imei") final String imei) throws ExecutionException {
        Session session = new Session(imei, -1);
        SessionCache.getInstance().put(imei, session);
        return session;
    }

    @POST
    @Path("update")
    @Consumes("application/json")
    public void updateRestaurant(@HeaderParam("imei") final String imei,
                                 Restaurant restaurant) {
        restaurantDao.update(restaurant);
    }

    @POST
    @Path("read")
    @Produces("application/json")
    @Consumes("text/plain")
    public Restaurant readRestaurantByEmail(String email) {
        return ((RestaurantDaoImpl) restaurantDao).readByEmail(email);
    }

    @POST
    @Path("read/id")
    @Produces("application/json")
    @Consumes("application/json")
    public Restaurant readRestaurant(final String id) {
        return restaurantDao.read(Long.parseLong(id));
    }

    @POST
    @Path("read/rates/{startIndex}/{count}")
    @Consumes("application/json")
    @Produces("application/json")
    public List<SellingRate> getRates(@HeaderParam("imei") final String imei,
                                      @PathParam("startIndex") int startIndex,
                                      @PathParam("count") int count,
                                      Location deviceLocation) throws ExecutionException {
        Session session = SessionCache.getInstance().get(imei);
        // restroId and userId are same for now
        long restroId = session.getUserId();

        // Restaurant can be made part of session
        Restaurant restaurant = restaurantDao.read(restroId);
        Location restaurantLocation = restaurant.readLocation() != null ? restaurant.readLocation() : deviceLocation;
        Warehouse nearestWarehouse = ((WarehouseDaoImpl) warehouseDao).findNearest(restaurantLocation);
        return ((RestaurantDaoImpl) restaurantDao).readSellingRates(
                Arrays.asList(restroId, nearestWarehouse.getId()),
                new Date(System.currentTimeMillis()), startIndex, count);
    }

    /**
     * Get selling rates for a restaurant.
     * The list will contain {@link SellingRate}s only for articles, those have id present in articleIds set.
     *
     * @param imei       imei of the device
     * @param startIndex startIndex of page
     * @param count      count of instances to be sent
     * @param filter     filtered artilceIds for which rates are to be fetched
     * @return {@link List} of selling rates for articles given with article id set.
     * @throws ExecutionException <p/>
     *                            Note : Pagination is ignored for now, is yet to be done.
     */
    @POST
    @Path("read/rates/filtered/{startIndex}/{count}")
    @Consumes("application/json")
    @Produces("application/json")
    public List<SellingRate> getRates(@HeaderParam("imei") final String imei,
                                      @PathParam("startIndex") final int startIndex,
                                      @PathParam("count") final int count,
                                      Filter filter)
            throws ExecutionException, SQLException, IOException, ClassNotFoundException {

        logger.log(Level.INFO, String.valueOf(System.currentTimeMillis()));
        // TODO: 04/08/16 cache on filter result for paging purpose
        List<SellingRate> rates = getFilteredRates(imei, filter);
        if (startIndex >= rates.size()) {
            return new ArrayList<SellingRate>();
        }
        int endIndex = startIndex + count;
        if (endIndex > rates.size()) {
            endIndex = rates.size();
        }
        return rates.subList(startIndex, endIndex);
    }

    /**
     * Get list of all SellingRates with given filter conditions.
     *
     * @param filter filter to narrow search
     * @return list of sellingRates
     * @throws ExecutionException
     */
    private List<SellingRate> getFilteredRates(String imei, final Filter filter) throws ExecutionException, SQLException, IOException, ClassNotFoundException {

        Session session = SessionCache.getInstance().get(imei);
        // restroId and userId are same for now
        final long restroId = session.getUserId();
        // Restaurant can be made part of session
        Restaurant restaurant = restaurantDao.read(restroId);
        Location restaurantLocation = restaurant != null && restaurant.readLocation() != null ? restaurant.readLocation() : filter.getLocation();
        List<Long> salePointIds = new ArrayList<Long>() {{
            add(restroId);
        }};

        Warehouse nearestWarehouse = ((WarehouseDaoImpl) warehouseDao).findNearest(restaurantLocation);
        if (nearestWarehouse != null) salePointIds.add(nearestWarehouse.getId());
        List<SaleRateDaoImpl.SellingRateRow> exhaustiveSellingRates = ((SaleRateDaoImpl) sellingRateDao).listRows(salePointIds, new Date(System.currentTimeMillis()));

        ArticleCache articleCahche = ArticleCache.getInstance();
        logger.log(Level.INFO, String.valueOf(System.currentTimeMillis()));
        List<Long> idsArticlesFilteredByName = getArticleIds(filter.getName());
        HashSet<Long> idSet = new HashSet<Long>(idsArticlesFilteredByName);
        Map<Long, SellingRate> filteredSellingRates = new HashMap<Long, SellingRate>();
        for (SaleRateDaoImpl.SellingRateRow rate : exhaustiveSellingRates) {
            SellingRate sellingRate = new SellingRate();
            sellingRate.setPrice(rate.getPrice());
            sellingRate.setStartDate(rate.getStartDate());
            sellingRate.setEndDate(rate.getEndDate());
            sellingRate.setArticle(articleCahche.get(rate.getArticleId()));
            if (idSet.contains(rate.getArticleId())) {
                if (!filteredSellingRates.containsKey(rate.getArticleId())) {
                    filteredSellingRates.put(rate.getArticleId(), sellingRate);
                } else {
                    if (rate.getSalepointId() == restroId) {
                        filteredSellingRates.put(rate.getArticleId(), sellingRate);
                    }
                }
            }
        }

        return new ArrayList<SellingRate>(filteredSellingRates.values());
    }

    @POST
    @Path("/order")
    @Consumes("application/json")
    @Produces("application/json")
    public Response createOrder(Order order) {

        Serializable id = orderDao.create(order);
        if (id != null) {
            return Response.ok("order submitted successfully").build();
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
}
