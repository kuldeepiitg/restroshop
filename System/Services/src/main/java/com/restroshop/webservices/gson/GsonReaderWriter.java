package com.restroshop.webservices.gson;

import com.google.gson.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.sql.Timestamp;

/**
 * Gson reader writer to serialize and deserialize POJOs across network calls.
 * <p/>
 * Because android application can have only Gson deserializer so it is better to use same both side.
 * In some of cases jackson and gson serializer were out of phase, like in Timestamp.
 * <p>
 * Created by Kuldeep Yadav on 12/08/16.
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GsonReaderWriter<T> implements MessageBodyReader<T>, MessageBodyWriter<T> {

    /**
     * Google json serializer
     */
    private final Gson gson;

    @Context
    private UriInfo ui;

    public GsonReaderWriter() {
        // add new type adapters here
        GsonBuilder builder = new GsonBuilder()
                .serializeNulls()
                .enableComplexMapKeySerialization()
                .setDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSSSSS");
        builder.registerTypeAdapter(Timestamp.class, new JsonDeserializer<Timestamp>() {

            public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return Timestamp.valueOf(json.getAsString());
            }
        });

        this.gson = builder.create();
    }

    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    public T readFrom(Class<T> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException, WebApplicationException {

        InputStreamReader reader = new InputStreamReader(entityStream, "UTF-8");
        try {
            return gson.fromJson(reader, type);
        } finally {
            reader.close();
        }
    }

    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    public long getSize(T t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    public void writeTo(T t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        PrintWriter printWriter = new PrintWriter(entityStream);
        try {
            String json;
            json = gson.toJson(t);
            printWriter.write(json);
            printWriter.flush();
        } finally {
            printWriter.close();
        }
    }
}
