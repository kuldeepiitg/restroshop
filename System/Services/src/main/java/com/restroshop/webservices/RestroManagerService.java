package com.restroshop.webservices;

import com.restroshop.dao.EntityDao;
import com.restroshop.dao.RestroManagerDaoImpl;
import com.restroshop.dao.model.RestroManager;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * CRUD services for {@link RestroManager}
 * <p>
 * Created by Kuldeep Yadav on 02/07/16.
 */
@Path("/restromanager")
public class RestroManagerService {

    private EntityDao<RestroManager> restroManagerDao = new RestroManagerDaoImpl();

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public RestroManager create(RestroManager user) {

        // user will be asked to set password explicitly
        // this value is to be empty string.
        user.setPassword(null);

        Long id = ((Long) restroManagerDao.create(user));
        RestroManager savedUser = restroManagerDao.read(id);

        // Passing password to user is bad idea
        // because if someone know the API then he can simply know
        // password for anyone.
        savedUser.setPassword(null);
        return savedUser;
    }
}
