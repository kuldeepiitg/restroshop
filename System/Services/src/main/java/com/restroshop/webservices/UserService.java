package com.restroshop.webservices;

import com.restroshop.dao.EntityDao;
import com.restroshop.dao.UserDaoImpl;
import com.restroshop.dao.model.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Services related to user
 * <p>
 * Created by Kuldeep Yadav on 01-Jul-16.
 */
@Path("/user")
public class UserService {

    EntityDao<User> userDao = new UserDaoImpl();

    @GET
    public Response greet() {
        return Response.ok("Welcome to restroshop APIs...").build();
    }

    @GET
    @Path("/read/{id}")
    @Produces("application/json")
    public Response readUser(@PathParam("id") final long id) {

        User user = userDao.read(id);
        return user != null ?
                Response.ok(user, MediaType.APPLICATION_JSON).build() :
                Response.noContent().build();
    }

    @GET
    @Path("/read")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> list() {
        return userDao.list();
    }

    @PUT
    @Path("/update")
    public Response update(User user) {
        userDao.update(user);
        return Response.ok("User updated successfully").build();
    }

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public User create(User user) {
        user.removePassword();
        Long id = ((Long) userDao.create(user));
        User savedUser = userDao.read(id);
        savedUser.setPassword(null);
        return savedUser;
    }

    @DELETE
    @Path("/delete/{id}")
    public Response delete(@PathParam("id") final long id) {
        userDao.delete(id);
        return Response.ok("User deleted successfully").build();
    }
}
