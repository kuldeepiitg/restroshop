package com.restroshop.webservices.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.restroshop.dao.ArticleDaoImpl;
import com.restroshop.dao.model.Article;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Article collectionCache
 * <p>
 * Created by Kuldeep Yadav on 04/08/16.
 */
public class ArticleCache {

    /**
     * Singleton instance of {@link ArticleCache}
     */
    private static ArticleCache articleCache;
    /**
     * Entity cache size
     */
    private final int ENTITY_CACHE_SIZE = 2000;
    /**
     * Given a entity name as key it contains exhaustive list of entity.
     */
    private LoadingCache<String, List<Article>> collectionCache;
    /**
     * Cache storing article entity against id as key
     */
    private LoadingCache<Long, Article> entityCache;
    /**
     * Article dao instance
     */
    private ArticleDaoImpl articleDao;

    private ArticleCache() {
        articleDao = new ArticleDaoImpl();
        this.collectionCache = CacheBuilder.newBuilder().maximumSize(1).refreshAfterWrite(1, TimeUnit.HOURS).build(new CacheLoader<String, List<Article>>() {
            @Override
            public List<Article> load(String key) throws Exception {
                return articleDao.list();
            }
        });
        this.entityCache = CacheBuilder.newBuilder().maximumSize(ENTITY_CACHE_SIZE).build(new CacheLoader<Long, Article>() {
            @Override
            public Article load(Long id) throws Exception {
                return articleDao.read(id);
            }
        });
    }

    /**
     * Get singleton instance.
     *
     * @return ArticleCache
     */
    public static ArticleCache getInstance() {
        if (articleCache == null) {
            articleCache = new ArticleCache();
        }
        return articleCache;
    }

    /**
     * Get list of articles cached in memory.
     *
     * @return article list
     * @throws ExecutionException
     */
    public List<Article> get() throws ExecutionException {
        return collectionCache.get(Article.class.getName());
    }

    /**
     * Get article for given id
     *
     * @param id id of Article
     * @return Article
     * @throws ExecutionException
     */
    public Article get(long id) throws ExecutionException {
        return entityCache.get(id);
    }
}
