package com.restroshop.session;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.restroshop.dao.EntityDao;
import com.restroshop.dao.SessionDaoImpl;
import com.restroshop.dao.model.Session;

import java.io.Serializable;
import java.util.concurrent.ExecutionException;

/**
 * Cache for session.
 * <p>
 * Created by Kuldeep Yadav on 10/07/16.
 */
public class SessionCache {

    private static SessionCache sessionCache;
    /**
     * Session dao
     */
    private EntityDao<Session> dao;
    /**
     * Cache to store logged in session
     */
    private LoadingCache<String, Session> cache;

    private SessionCache() {
        dao = new SessionDaoImpl();
        cache = CacheBuilder.newBuilder().maximumSize(100).build(new CacheLoader<String, Session>() {
            @Override
            public Session load(String imei) throws Exception {
                Session session = ((SessionDaoImpl) dao).read(imei);
                if (session != null) {
                    return session;
                } else {
                    throw new Exception("session for " + imei + " not available in database");
                }
            }
        });
    }

    /**
     * Singleton instance of {@link SessionCache}.
     *
     * @return instance of {@link SessionCache}
     */
    public static SessionCache getInstance() {

        if (sessionCache == null) {
            sessionCache = new SessionCache();
        }
        return sessionCache;
    }

    /**
     * Set session for device specified by imei.
     *
     * @param imei    device IMEI
     * @param session user session
     */
    public void put(String imei, Session session) {
        Session sessionInDB = ((SessionDaoImpl) dao).read(imei);
        if (sessionInDB != null) {
            dao.delete(sessionInDB.getId());
        }
        Serializable id = dao.create(session);
        cache.put(imei, dao.read(id));
    }

    /**
     * Get session for device identified by IMEI
     *
     * @param imei IMEI of device
     * @return Session
     */
    public Session get(String imei) throws ExecutionException {
        return cache.get(imei);
    }

    /**
     * Check if given details belong to an active session.
     *
     * @param imei imei of device from which request comming
     * @param id   id of logged in user
     * @return true if user have an active session.
     * @throws ExecutionException
     */
    public boolean loggedIn(String imei, long id) throws ExecutionException {
        Session session = cache.get(imei);
        return session != null && session.getUserId() == id;
    }
}
