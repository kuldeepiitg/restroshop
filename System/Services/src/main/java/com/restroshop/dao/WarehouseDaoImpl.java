package com.restroshop.dao;

import com.restroshop.dao.hibernate.HibernateSessionProvider;
import com.restroshop.dao.model.Location;
import com.restroshop.dao.model.Warehouse;
import com.restroshop.utilities.MapUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Hibernate DAO implementation for {@link Warehouse}.
 * <p>
 * Created by kuldeep on 21/07/16.
 */
public class WarehouseDaoImpl extends EntityDao<Warehouse> {

    /**
     * Ware house lookup radius
     */
    private int WAREHOUSE_LOOKUP_RADIUS = 50 * 1000;

    public Serializable create(Warehouse warehouse) {
        return super.create(warehouse);
    }

    /**
     * Get warehouse with given id.
     *
     * @param id id of the restaurant. It has to be of type {@link Long}.
     * @return warehouse with given id.
     */
    public Warehouse read(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return (Warehouse) session.get(Warehouse.class, id);
        } finally {
            session.close();
        }
    }

    public List<Warehouse> list() {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return session.createCriteria(Warehouse.class).list();
        } finally {
            session.close();
        }
    }

    public List<Warehouse> list(int startIndex, int count) {
        throw new UnsupportedOperationException("paging is not yet implemneted");
    }

    public void update(Warehouse warehouse) {
        super.update(warehouse);
    }

    /**
     * Delete warehouse with given id from system.
     *
     * @param id of the entity.
     *           It must have to be of type {@link Long}
     */
    public void delete(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            Warehouse warehouse = read(id);
            session.beginTransaction();
            session.delete(warehouse);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    /**
     * Find {@link Warehouse}s nearby location with in lookupRadius.
     *
     * @param location     location near by which {@link Warehouse}s are to be searched
     * @param lookupRadius radius for looking up warehouses. Warehouses outside this radius will be ignored.
     * @return {@link List} of {@link Warehouse}s located near by location with in lookup radius. List is sorted
     * on distance from location.
     */
    public List<Warehouse> findNearBy(Location location, int lookupRadius) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();

        try {
            Criteria criteria = session.createCriteria(Warehouse.class);
            MapUtils.Pair<Double> longitudeBound = MapUtils.getLongitudeBounds(location, lookupRadius);
            criteria.add(Restrictions.ge("longitude", longitudeBound.getFirst()));
            criteria.add(Restrictions.le("longitude", longitudeBound.getSecond()));
            MapUtils.Pair<Double> latitudeBound = MapUtils.getLatitudeBounds(location, lookupRadius);
            criteria.add(Restrictions.ge("latitude", latitudeBound.getFirst()));
            criteria.add(Restrictions.le("latitude", latitudeBound.getSecond()));

            // Warehouses in square formed by latlng bounds.
            List<Warehouse> warehousesInSquare = criteria.list();
            Comparator<Warehouse> comparator = new MapUtils.DisplacementComparator(location);
            Collections.sort(warehousesInSquare, comparator);
            int farthestWarehouseWithInBoundIndex = warehousesInSquare.size() - 1;
            for (int i = warehousesInSquare.size() - 1; i >= 0; i--) {
                if (MapUtils.displacement(warehousesInSquare.get(i).readLocation(), location) <= lookupRadius) {
                    farthestWarehouseWithInBoundIndex = i;
                    break;
                }
            }
            return warehousesInSquare.subList(0, farthestWarehouseWithInBoundIndex + 1);
        } finally {
            session.close();
        }
    }

    /**
     * Find nearest warehouse from the location with in lookup radius.
     *
     * @param location     Location (of restaurant) for which warehouse is to be found.
     * @param lookupRadius Radius for looking up warehouses.
     * @return nearest warehouse with in lookup radius or null if not available.
     */
    public Warehouse findNearest(Location location, int lookupRadius) {
        List<Warehouse> nearByWarehouses = findNearBy(location, lookupRadius);
        return nearByWarehouses.size() > 0 ? nearByWarehouses.get(0) : null;
    }

    /**
     * Find nearest warehouse from location with in {@link WarehouseDaoImpl#WAREHOUSE_LOOKUP_RADIUS}.
     *
     * @param location location nearby which warehouse is to be found
     * @return nearest warehouse with in lookup radius or null if not available.
     */
    public Warehouse findNearest(Location location) {
        return findNearest(location, WAREHOUSE_LOOKUP_RADIUS);
    }
}