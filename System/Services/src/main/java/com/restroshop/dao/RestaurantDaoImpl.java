package com.restroshop.dao;

import com.restroshop.dao.hibernate.HibernateSessionProvider;
import com.restroshop.dao.model.Restaurant;
import com.restroshop.dao.model.SellingRate;
import org.hibernate.Query;
import org.hibernate.Session;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

/**
 * Hibernate DAO for {@link Restaurant}
 * <p/>
 * Created by Kuldeep Yadav on 27-Jun-16.
 */
public class RestaurantDaoImpl extends EntityDao<Restaurant> {

    @Override
    public Long create(Restaurant restaurant) {
        return (Long) super.create(restaurant);
    }

    /**
     * Get Restaurant with given id.
     *
     * @param id id of the entity. It have to be of type {@link Long}.
     * @return restaurant with given id.
     */
    @Override
    public Restaurant read(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return (Restaurant) session.get(Restaurant.class, id);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Restaurant> list() {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return session.createCriteria(Restaurant.class).list();
        } finally {
            session.clear();
        }
    }

    @Override
    public List<Restaurant> list(int startIndex, int count) {
        throw new UnsupportedOperationException("paging is not yet implemneted");
    }

    @Override
    public void update(Restaurant restaurant) {
        super.update(restaurant);
    }

    /**
     * Delete Restaurant with given id. It have to be of type {@link Long}
     *
     * @param id of the entity.
     */
    @Override
    public void delete(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            Restaurant restaurant = read(id);
            session.beginTransaction();
            session.delete(restaurant);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    /**
     * Get restaurant with given email id.
     *
     * @param email email id of restaurant
     * @return Restaurant with given email id
     */
    public Restaurant readByEmail(String email) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            @SuppressWarnings("JpaQlInspection")
            Query query = session.createQuery("from Restaurant where email LIKE :email");
            query.setParameter("email", email);
            List<Restaurant> list = query.list();
            assert list.size() == 1;
            if (list.size() == 1) {
                return list.get(0);
            } else if (list.size() == 0) {
                return null;
            } else {
                throw new RuntimeException("Unique restautant doesn't exists with given email");
            }
        } finally {
            session.close();
        }
    }

    /**
     * Read selling rate page for given sale points on specified date.
     *
     * @param salePointIds id of {@link com.restroshop.dao.model.SalePoint}
     * @param date         date
     * @param startIndex   first index of page in system
     * @param count        number of instances to be fetched from system
     * @return {@link List} of {@link SellingRate} for given sale points on specified date.
     */
    public List<SellingRate> readSellingRates(List<Long> salePointIds, Date date, int startIndex, int count) {

        Query query = buildQuery(salePointIds, date);
        query.setFirstResult(startIndex);
        query.setMaxResults(count);
        return query.list();
    }

    /**
     * Build query with salepoints and date constraints
     *
     * @param salePointIds id of {@link com.restroshop.dao.model.SalePoint}
     * @param date         date
     * @return {@link Query} for {@link SellingRate}s.
     */
    private Query buildQuery(List<Long> salePointIds, Date date) {
        // TODO: 03/08/16 Figure out Criteria use
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("FROM SellingRate ");
        queryBuilder.append("WHERE startDate <= :date ");
        queryBuilder.append("AND endDate >= :date ");
        if (salePointIds.size() > 0) {
            queryBuilder.append("AND");
            queryBuilder.append(createConditionsForSalePointIds(salePointIds));
        }
        Session session = HibernateSessionProvider.getSessionFactory().openSession();

        try {
            Query query = session.createQuery(queryBuilder.toString());
            query.setParameter("date", date);
            return query;
        } finally {
            session.close();
        }
    }

    /**
     * Create conditions for salePoint ids in disjunction.
     *
     * @param salePointIds salepoint ids
     * @return conditions with salepoint ids in disjunction
     */
    private String createConditionsForSalePointIds(List<Long> salePointIds) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < salePointIds.size(); i++) {
            if (i != 0) {
                builder.append(" OR");
            }
            builder.append(" salePoint.id = " + salePointIds.get(i));
        }
        return builder.toString();
    }

    /**
     * Read selling rates for given sale points on specified date.
     *
     * @param salePointIds ids of {@link com.restroshop.dao.model.SalePoint}
     * @param date         date
     * @return {@link List} of {@link SellingRate} for given sale points on specified date.
     */
    public List<SellingRate> readSellingRates(List<Long> salePointIds, Date date) {

        return buildQuery(salePointIds, date).list();
    }
}
