package com.restroshop.dao;

import com.restroshop.dao.hibernate.HibernateSessionProvider;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

/**
 * Basic CRUD operations supported by all entities which are
 * instance of {@code com.restroshop.dao.model.BaseEntity}.
 * <p/>
 * Created by Kuldeep Yadav on 24-Jun-16.
 */
public abstract class EntityDao<E> {

    /**
     * Create entity
     *
     * @return id of the entity saved in database
     */
    public Serializable create(E entity) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Serializable id = session.save(entity);
            session.getTransaction().commit();
            return id;
        } finally {
            session.close();
        }
    }

    /**
     * Read an entity.
     *
     * @param id id of the entity.
     * @return the entity
     */
    public abstract E read(Serializable id);

    /**
     * List all the instances of entities.
     *
     * @return all entities of type {@code E}
     */
    public abstract List<E> list();

    /**
     * List {@code count} number of entities starting from given {@code startIndex}
     *
     * @param startIndex index of first entity in list
     * @param count      count of entities in list
     * @return list containing desires entities
     */
    public abstract List<E> list(int startIndex, int count);

    /**
     * Update an existing entity.
     *
     * @param entity modified instance of entity.
     */
    public void update(E entity) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    /**
     * Delete an entity.
     *
     * @param id of the entity.
     */
    public abstract void delete(Serializable id);
}
