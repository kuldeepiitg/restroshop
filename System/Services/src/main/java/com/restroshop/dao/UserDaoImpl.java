package com.restroshop.dao;

import com.restroshop.dao.hibernate.HibernateSessionProvider;
import com.restroshop.dao.model.User;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

/**
 * Hibernate dao implementation for users.
 * <p>
 * Created by Kuldeep Yadav on 30-Jun-16.
 */
public class UserDaoImpl extends EntityDao<User> {

    /**
     * Hibernate session.
     */
    private Session session = HibernateSessionProvider.getSessionFactory().openSession();

    @Override
    public Serializable create(User user) {
        return super.create(user);
    }

    @Override
    public User read(Serializable id) {
        return (User) session.get(User.class, id);
    }

//    /**
//     * Get a user by email id
//     *
//     * @param email email id of user
//     * @return User with given email.
//     */
//    public User read(String email) {
//        Query query = session.createQuery("from User where email LIKE :email");
//        query.setParameter("email", email);
//        List<User> list = query.list();
//        return list.get(0);
//    }

    @Override
    public List<User> list() {
        return session.createCriteria(User.class).list();
    }

    @Override
    public List<User> list(int startIndex, int count) {
        return null;
    }

    @Override
    public void update(User user) {
        session.beginTransaction();
        session.update(user);
        session.getTransaction().commit();
    }

    @Override
    public void delete(Serializable id) {
        User user = read(id);
        session.beginTransaction();
        session.delete(user);
        session.getTransaction().commit();
    }

    /**
     * Shutdown session
     */
    public void shutdown() {
        session.close();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.session.close();
    }
}
