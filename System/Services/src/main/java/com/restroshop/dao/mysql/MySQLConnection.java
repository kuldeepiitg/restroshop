package com.restroshop.dao.mysql;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Connection to mysql database using JDBC connectors.
 * <p>
 * Created by Kuldeep Yadav on 05/08/16.
 */
public class MySQLConnection {

    /**
     * Mysql connection
     */
    private Connection connection;

    public MySQLConnection() throws ClassNotFoundException, SQLException, IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = new FileInputStream(classLoader.getResource("mysql.properties").getFile());
        Properties properties = new Properties();
        properties.load(inputStream);
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/dbrestro",
                        properties.getProperty("username"),
                        properties.getProperty("password"));
    }

    /**
     * Get sql connection.
     *
     * @return Connection
     */
    public Connection getConnection() {
        return this.connection;
    }

    /**
     * Close mysql connection
     *
     * @throws SQLException
     */
    public void close() throws SQLException {
        this.connection.close();
    }
}
