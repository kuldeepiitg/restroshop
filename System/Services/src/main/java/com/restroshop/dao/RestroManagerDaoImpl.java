package com.restroshop.dao;

import com.restroshop.dao.hibernate.HibernateSessionProvider;
import com.restroshop.dao.model.RestroManager;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

/**
 * Hibernate DAO for {@link RestroManager}
 * <p/>
 * Created by Kuldeep Yadav on 28-Jun-16.
 */
public class RestroManagerDaoImpl extends EntityDao<RestroManager> {

    @Override
    public Serializable create(RestroManager restroManager) {
        return super.create(restroManager);
    }

    /**
     * Get {@link RestroManager} with given id. Id have to be of type {@link RestroManager}.
     *
     * @param id id of the entity
     * @return Restaurant manager with given id
     */
    @Override
    public RestroManager read(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return (RestroManager) session.get(RestroManager.class, id);
        } finally {
            session.close();
        }
    }

    @Override
    public List<RestroManager> list() {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return session.createCriteria(RestroManager.class).list();
        } finally {
            session.close();
        }
    }

    public List<RestroManager> list(int startIndex, int count) {
        return null;
    }

    @Override
    public void update(RestroManager restroManager) {
        super.update(restroManager);
    }

    /**
     * Delete {@link RestroManager} with given id. Id have to be of type {@link Long}.
     *
     * @param id of the entity.
     */
    @Override
    public void delete(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            RestroManager restroManager = read(id);
            session.beginTransaction();
            session.delete(restroManager);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }
}
