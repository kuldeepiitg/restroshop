package com.restroshop.dao;

import com.restroshop.dao.hibernate.HibernateSessionProvider;
import com.restroshop.dao.model.Order;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

/**
 * Hibernate Dao implementation for {@link Order}.
 * <p>
 * Created by Kuldeep Yadav on 12/08/16.
 */
public class OrderDaoImpl extends EntityDao<Order> {

    @Override
    public Long create(Order order) {
        return (Long) super.create(order);
    }

    @Override
    public Order read(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return (Order) session.get(Order.class, id);
        } finally {
            session.close();
        }
    }

    public List<Order> list() {
        throw new UnsupportedOperationException("list is not supported for order");
    }

    public List<Order> list(int startIndex, int count) {
        throw new UnsupportedOperationException("list is not supported for order");
    }

    public void update(Order entity) {
        throw new UnsupportedOperationException("update is not supported for order");
    }

    public void delete(Serializable id) {
        throw new UnsupportedOperationException("delete is not supported for order");
    }
}
