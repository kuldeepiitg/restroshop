package com.restroshop.dao;

import com.restroshop.dao.hibernate.HibernateSessionProvider;
import com.restroshop.dao.model.PurchaseRate;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Hibernate implementation of DAO for {@link PurchaseRate}
 * <p>
 * Created by Kuldeep Yadav on 11-Jul-16.
 */
public class PurchaseRateDaoImpl extends EntityDao<PurchaseRate> {

    @Override
    public Long create(PurchaseRate rate) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Serializable id = session.save(rate);
            session.getTransaction().commit();
            return (Long) id;
        } finally {
            session.close();
        }
    }

    @Override
    public PurchaseRate read(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return (PurchaseRate) session.get(PurchaseRate.class, id);
        } finally {
            session.close();
        }
    }

    @Override
    public List<PurchaseRate> list() {
        throw new UnsupportedOperationException("List operations are not supported for Purchase Rate");
    }

    @Override
    public List<PurchaseRate> list(int startIndex, int count) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(PurchaseRate.class);
            criteria.setFirstResult(startIndex);
            criteria.setMaxResults(count);
            return criteria.list();
        } finally {
            session.close();
        }
    }

    public List<PurchaseRate> list(int startIndex, int count, Date date) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(PurchaseRate.class);
            criteria.add(Restrictions.eq("date", date));
            criteria.setFirstResult(startIndex);
            criteria.setMaxResults(count);
            return criteria.list();
        } finally {
            session.close();
        }
    }

    @Override
    public void update(PurchaseRate rate) {
        super.update(rate);
    }

    @Override
    public void delete(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            PurchaseRate rate = read(id);
            session.beginTransaction();
            session.delete(rate);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    /**
     * Read Purchase rate of given article on specified date
     *
     * @param articleId id of article
     * @param date date for which price is to be checked
     * @return {@link PurchaseRate} of the article on the date
     */
    public PurchaseRate read(long articleId, Date date) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            String sql = "SELECT * FROM PURCHASE_RATE WHERE article_id = :a_id AND date = :date";
            SQLQuery query = session.createSQLQuery(sql);
            query.addEntity(PurchaseRate.class);
            query.setParameter("a_id", articleId);
            query.setParameter("date", date);
            List<PurchaseRate> result = query.list();
            assert result.size() == 1;
            if (result.size() == 1)
                return result.get(0);
            else
                return null;
        } finally {
            session.close();
        }
    }
}
