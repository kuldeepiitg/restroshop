package com.restroshop.dao;

import com.restroshop.dao.hibernate.HibernateSessionProvider;
import com.restroshop.dao.model.Session;
import org.hibernate.Query;

import java.io.Serializable;
import java.util.List;

/**
 * DAO implementation of Session in hibernate.
 * <p>
 * Created by Kuldeep Yadav on 01/08/16.
 */
public class SessionDaoImpl extends EntityDao<Session> {

    @Override
    public Long create(Session session) {
        return (Long) super.create(session);
    }

    @Override
    public Session read(Serializable id) {
        org.hibernate.Session hibernateSession = HibernateSessionProvider.getSessionFactory().openSession();

        try {
            return (Session) hibernateSession.get(Session.class, id);
        } finally {
            hibernateSession.close();
        }
    }

    /**
     * Read a session by imei.
     *
     * @param imei imei of device.
     * @return Session on device.
     */
    public Session read(String imei) {
        org.hibernate.Session hibernateSession = HibernateSessionProvider.getSessionFactory().openSession();

        try {
            Query query = hibernateSession.createQuery("from Session where imei LIKE :imei");
            query.setParameter("imei", imei);
            List<Session> list = query.list();
            if (list.size() == 1) {
                return list.get(0);
            } else if (list.size() == 0) {
                return null;
            } else {
                throw new RuntimeException("Inconsistent : More than one session exists for a device");
            }
        } finally {
            hibernateSession.close();
        }
    }

    @Override
    public List<Session> list() {
        org.hibernate.Session hibernateSession = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return hibernateSession.createCriteria(Session.class).list();
        } finally {
            hibernateSession.close();
        }
    }

    @Override
    public List<Session> list(int startIndex, int count) {
        throw new UnsupportedOperationException("list for session is not yet supported");
    }

    @Override
    public void update(Session session) {
        super.update(session);
    }

    @Override
    public void delete(Serializable id) {
        org.hibernate.Session hibernateSession = HibernateSessionProvider.getSessionFactory().openSession();

        try {
            Session session = read(id);
            hibernateSession.beginTransaction();
            hibernateSession.delete(session);
            hibernateSession.getTransaction().commit();
        } finally {
            hibernateSession.close();
        }
    }
}
