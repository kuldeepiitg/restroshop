package com.restroshop.dao;

import com.restroshop.dao.hibernate.HibernateSessionProvider;
import com.restroshop.dao.model.SellingRate;
import com.restroshop.dao.mysql.MySQLConnection;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Hibernate DAO implementation for {@link SellingRate}
 * <p/>
 * Created by Kuldeep Yadav on 27-Jun-16.
 */
public class SaleRateDaoImpl extends EntityDao<SellingRate> {

    @Override
    public SellingRate create(SellingRate rate) {
        return (SellingRate) super.create(rate);
    }

    /**
     * Get rate with given id.
     *
     * @param id id of the entity. It have to be {@link SellingRate}.
     * @return rate with given id.
     */
    @Override
    public SellingRate read(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return (SellingRate) session.get(SellingRate.class, id);
        } finally {
            session.close();
        }
    }

    @Override
    public List<SellingRate> list() {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            return session.createCriteria(SellingRate.class).list();
        } finally {
            session.close();
        }
    }

    @Override
    public List<SellingRate> list(int startIndex, int count) {
        throw new UnsupportedOperationException("sublist of sales rate is not implemented");
    }

    @Override
    public void update(SellingRate rate) {
        super.update(rate);
    }

    /**
     * Delete {@link SellingRate} with given id. It must have to be of type {@link SellingRate}.
     *
     * @param id of the entity.
     */
    @Override
    public void delete(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            SellingRate sellingRate = read(id);
            session.beginTransaction();
            session.delete(sellingRate);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    /**
     * Read rate with given {@link com.restroshop.dao.model.Article#id}
     * and {@link com.restroshop.dao.model.SalePoint#id}
     * <p/>
     *
     * @param articleId   id of {@link com.restroshop.dao.model.Article}, {@link SellingRate} belongs to
     * @param salePointId id of {@link com.restroshop.dao.model.SalePoint}, {@link SellingRate} belongs to.
     * @param date date on which sale rate is to be found
     * @return rate with given {@link com.restroshop.dao.model.Article#id}
     * and {@link com.restroshop.dao.model.SalePoint#id}
     */
    public SellingRate read(long articleId, long salePointId, Date date) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            String sql = "SELECT * FROM SELLING_RATE WHERE article_id = :a_id AND salepoint_id = :s_id AND start_date <= :date AND end_date > :date";
            SQLQuery query = session.createSQLQuery(sql);
            query.addEntity(SellingRate.class);
            query.setParameter("a_id", articleId);
            query.setParameter("s_id", salePointId);
            query.setParameter("date", date);
            List<SellingRate> results = query.list();
            assert results.size() == 1;
            if (results.size() == 1) {
                return results.get(0);
            } else {
                return null;
            }
        } finally {
            session.close();
        }
    }

    /**
     * Read today's rate with given {@link com.restroshop.dao.model.Article#id}
     * and {@link com.restroshop.dao.model.SalePoint#id}
     * <p/>
     *
     * @param articleId   id of Article.
     * @param salePointId id of sale point.
     * @return rate with given article and sale point.
     */
    public SellingRate read(long articleId, long salePointId) {
        return read(articleId, salePointId, new Date(System.currentTimeMillis()));
    }

    /**
     * List rows
     *
     * @return List of all rows of mysql table SELLING_RATE
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public List<SellingRateRow> listRows() throws SQLException, IOException, ClassNotFoundException {
        MySQLConnection mySQLConnection = new MySQLConnection();
        PreparedStatement statement = mySQLConnection.getConnection().prepareStatement("SELECT * FROM dbrestro.SELLING_RATE");
        ResultSet result = statement.executeQuery();
        List<SellingRateRow> list = new ArrayList<SellingRateRow>();
        while (result.next()) {
            SellingRateRow rate = new SellingRateRow(
                    result.getInt("artilce_id"),
                    result.getInt("salepoint_id"),
                    result.getInt("price"),
                    result.getDate("start_date"),
                    result.getDate("end_date"));
            list.add(rate);
        }
        mySQLConnection.getConnection().close();
        return list;
    }

    /**
     * Build conditions for salepoint ids in disjunction.
     *
     * @param salepointIds list of salepoint ids
     * @return String repersenting salepoint id constraints
     */
    private String buildSalepointConditions(List<Long> salepointIds) {

        StringBuilder builder = new StringBuilder();
        builder.append("(");
        for (int i = 0; i < salepointIds.size(); i++) {
            if (i != 0) {
                builder.append(" OR");
            }
            builder.append(" salepoint_id = " + salepointIds.get(i));
        }
        builder.append(")");
        return builder.toString();
    }

    /**
     * List rows of SELLING_RATE table with constraints.
     *
     * @param salePointIds list of salepoint ids
     * @param date         date
     * @return all rows having salepoint id in given list and
     * date falling in interval [start_date, end_date]
     * (inclusive both ends).
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public List<SellingRateRow> listRows(List<Long> salePointIds, Date date)
            throws SQLException, IOException, ClassNotFoundException {
        MySQLConnection mySQLConnection = new MySQLConnection();
        PreparedStatement statement = mySQLConnection.getConnection()
                .prepareStatement("SELECT * FROM dbrestro.SELLING_RATE WHERE start_date <= ? AND end_date >= ? AND"
                        + buildSalepointConditions(salePointIds));
        statement.setDate(1, date);
        statement.setDate(2, date);
        ResultSet result = statement.executeQuery();
        List<SellingRateRow> list = new ArrayList<SellingRateRow>();
        while (result.next()) {
            SellingRateRow rate = new SellingRateRow(
                    result.getInt("article_id"),
                    result.getInt("salepoint_id"),
                    result.getInt("price"),
                    result.getDate("start_date"),
                    result.getDate("end_date"));
            list.add(rate);
        }
        mySQLConnection.getConnection().close();
        return list;
    }

    /**
     * Row in SELLING_RATE table
     */
    public static class SellingRateRow {

        /**
         * Id of article associated with SellingRate
         * <p>
         * Foreign key to identify {@link com.restroshop.dao.model.Article}
         */
        private long articleId;

        /**
         * Id of salepoint associated with SellingRate
         * <p>
         * Foreign key to identify {@link com.restroshop.dao.model.SalePoint}
         */
        private long salepointId;

        /**
         * Price associated
         */
        private int price;

        /**
         * Start date of validity of {@link SellingRate}
         */
        private Date startDate;

        /**
         * End date of validity of {@link SellingRate}
         */
        private Date endDate;

        public SellingRateRow(long articleId,
                              long salepointId,
                              int price,
                              Date startDate,
                              Date endDate) {
            this.articleId = articleId;
            this.salepointId = salepointId;
            this.price = price;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public long getArticleId() {
            return articleId;
        }

        public void setArticleId(long articleId) {
            this.articleId = articleId;
        }

        public long getSalepointId() {
            return salepointId;
        }

        public void setSalepointId(long salepointId) {
            this.salepointId = salepointId;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public Date getStartDate() {
            return startDate;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }
    }
}
