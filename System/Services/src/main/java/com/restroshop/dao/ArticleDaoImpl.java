package com.restroshop.dao;

import com.restroshop.dao.hibernate.HibernateSessionProvider;
import com.restroshop.dao.model.Article;
import com.restroshop.dao.model.PurchaseRate;
import com.restroshop.dao.model.SalePointArticle;
import com.restroshop.dao.model.SellingRate;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Hibernate Dao for Article entity.
 * <p/>
 * Created by Kuldeep Yadav on 24-Jun-16.
 */
public class ArticleDaoImpl extends EntityDao<Article> {

    /**
     * Dao for rate.
     */
    private EntityDao<SellingRate> saleRateDao = new SaleRateDaoImpl();

    /**
     * Dao for purchase rate
     */
    private EntityDao<PurchaseRate> purchaseRateDao = new PurchaseRateDaoImpl();

    @Override
    public Long create(Article article) {
        return (Long) super.create(article);
    }

    /**
     * Get article with given id.
     *
     * @param id id of the entity. It have to be of type {@link Long}.
     * @return article with given id.
     */
    @Override
    public Article read(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();

        try {
            return (Article) session.get(Article.class, id);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Article> list() {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();

        try {
            return session.createCriteria(Article.class).list();
        } finally {
            session.close();
        }
    }

    @Override
    public List<Article> list(int startIndex, int count) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(Article.class);
            criteria.setFirstResult(startIndex);
            criteria.setMaxResults(count);
            return criteria.list();
        } finally {
            session.close();
        }
    }

    /**
     * Get list of articles, who are updated after given timestamp.
     *
     * @param lastUpdated {@link Timestamp}, which is freshness point for query.
     * @return Articles which are updated more recently than given timestamp.
     */
    public List<Article> list(Timestamp lastUpdated) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(Article.class);
            criteria.add(Restrictions.ge("lastUpdate", lastUpdated));
            return criteria.list();
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Article article) {
        super.update(article);
    }

    /**
     * Delete entity with given id.
     *
     * @param id of the entity. Must be of type {@link Long}
     */
    @Override
    public void delete(Serializable id) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            Article article = read(id);
            session.beginTransaction();
            session.delete(article);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    /**
     * Get an instance of SalePointArticle, that is article having rate for
     * the sale point, who is querying for it.
     *
     * @param articleId id of article
     * @param salePointId id of {@link com.restroshop.dao.model.SalePoint}
     * @param date date whose price is to be considered for selling rate
     *
     * @return {@link SalePointArticle} instance for article identified by articleId, SalePoint
     * is identified by salePointId and price are to be taken for the date
     */
    public SalePointArticle getSalePointArticle(long articleId, long salePointId, Date date) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            Article article = read(articleId);
            int rate;
            SellingRate sellingRate = ((SaleRateDaoImpl) saleRateDao).read(articleId, salePointId, date);
            if (sellingRate != null) {
                rate = sellingRate.getPrice();
                return new SalePointArticle(article, rate);
            }
            PurchaseRate purchaseRate = ((PurchaseRateDaoImpl) purchaseRateDao).read(articleId, date);
            if (purchaseRate != null) {
                return new SalePointArticle(article, purchaseRate.getPrice());
            }
            return null;
        } finally {
            session.close();
        }
    }


    /**
     * Get list of all article names.
     *
     * @return list of names of all articles
     */
    public List<String> names() {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(Article.class)
                    .setProjection(Projections.alias(Projections.groupProperty("name"), "name"));
            return criteria.list();
        } finally {
            session.close();
        }
    }

    /**
     * Get list of article having name starting with prefix
     *
     * @param prefix prefix
     * @return list of articles
     */
    public List<Article> findByName(String prefix) {
        Session session = HibernateSessionProvider.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(Article.class)
                    .add(Restrictions.like("name", prefix + "*"));
            return criteria.list();
        } finally {
            session.close();
        }
    }
}
