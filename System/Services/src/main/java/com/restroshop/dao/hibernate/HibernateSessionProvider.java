package com.restroshop.dao.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Hibernate session provider.
 * <p/>
 * Created by Kuldeep Yadav on 24-Jun-16.
 */
public class HibernateSessionProvider {

    /**
     * The session factory
     */
    private static final SessionFactory sessionFactory = buildSessionFactory();

    /**
     * Build session factory.
     *
     * @return session factory
     */
    private static SessionFactory buildSessionFactory() {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(
                    configuration.getProperties()).buildServiceRegistry();
            return configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * Get session factory instance.
     *
     * @return session factory.
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Gracefully close all sessions
     */
    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }
}
