package com.restroshop.authentication;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Utilities related to user authentication.
 * <p>
 * Created by Kuldeep Yadav on 30-Jun-16.
 */
public class AuthUtils {

    /**
     * Compute checksum.
     *
     * @param original original string
     * @return checksum of given string depending on algorithm.
     */
    public static String checksum(String original, ChecksumType algorithm) {

        MessageDigest md;
        try {
            md = MessageDigest.getInstance(algorithm.toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
        md.update(original.getBytes());

        StringBuilder stringBuilder = new StringBuilder();
        byte[] byteData = md.digest();
        for (byte byteValue : byteData) {
            stringBuilder.append(Integer.toString((byteValue & 0xff) + 0x100, 16).substring(1));
        }
        return stringBuilder.toString();
    }

    /**
     * Algorithm types available for checksum
     */
    public enum ChecksumType {
        // MD5
        MD5,

        // SHA-1
        SHA_1,

        // SHA-256
        SHA_256;

        @Override
        public String toString() {
            switch (this) {
                case MD5:
                    return "MD5";
                case SHA_1:
                    return "SHA-1";
                case SHA_256:
                    return "SHA-256";
            }
            throw new RuntimeException("No such algorithm exists");
        }
    }
}
