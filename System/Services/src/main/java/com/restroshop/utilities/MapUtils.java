package com.restroshop.utilities;

import com.restroshop.dao.model.Location;
import com.restroshop.dao.model.Warehouse;

import java.util.Comparator;

/**
 * All utilities related to Maps.
 * <p>
 * Created by Kuldeep Yadav on 22/07/16.
 * Taken from Pal-Up.
 */
public class MapUtils {

    /**
     * Radius of earth
     */
    private static final double EARTH_RADIUS = 6371 * 1000;

    /**
     * Displacement between two points on earth, considering it round
     * <p>
     * Note: It is prone to errors.
     * Hopefully error will be within 100, 500 and 1000 meters for
     * distance up to 1Km, 100Km and 1000Km respectively.
     *
     * @param point1 first Location
     * @param point2 second Location
     * @return displacement i.e. length of arch starting from point1 to point2 on greater circle.
     */
    public static int displacement(Location point1, Location point2) {

        double lat1 = point1.getLatitude();
        double lng1 = point1.getLongitude();

        double lat2 = point2.getLatitude();
        double lng2 = point2.getLongitude();

        Double latDistance = Math.toRadians(lat2 - lat1);
        Double lngDistance = Math.toRadians(lng2 - lng1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);
        Double angleInRadians = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (int) (EARTH_RADIUS * angleInRadians);
    }

    /**
     * Latitude bound around a point on map,
     * such that all the points not lying in latitude bound
     * must be located more than displacement bound.
     * Points inside the bound can fall with in range
     * or may be farther than displacement bound because
     * displacement depends on longitude too.
     *
     * @param Location          of a device/person
     * @param displacementBound bound on displacement(meter)
     * @return Pair bound (lower, upper) of latitude such that points having latitude outside the bound
     * must have displacement from Location greater than the bound.
     */
    public static Pair<Double> getLatitudeBounds(Location Location, int displacementBound) {

        return getLatitudeBounds(Location.getLatitude(), displacementBound);
    }

    /**
     * @param latitude          latitude of a device/person
     * @param displacementBound bound on displacement(meter)
     * @return Pair bound (lower, upper) of latitude such that points outside the bound
     * will always have distance greater than the displacement bound.
     */
    private static Pair<Double> getLatitudeBounds(double latitude, int displacementBound) {

        double deltaLatitude = displacementBound / EARTH_RADIUS * 360 / (2 * Math.PI);
        return new Pair<Double>(latitude - deltaLatitude, latitude + deltaLatitude);
    }

    /**
     * Longitude bound around a point on map,
     * such that all the points not lying in the bound
     * must be located more than the given displacement bound.
     * Points inside the bound can fall with in range
     * or may be farther than displacement bound.
     * <p>
     * This bound is computed by walking distance up to
     * displacementBound on both side of latitude circle.
     * The angle spawned on center of latitude circle is taken
     * as longitude range in consideration.
     * <p>
     * Note : This is loose bound because, ideally we should consider
     * walking on greater circle, which will be tilted thus
     * will spawn less longitude angle.
     *
     * @param Location          of a device/person
     * @param displacementBound bound on displacement(meter)
     * @return Pair bound (lower, upper) of longitude such that points having longitude outside the bound
     * must have distance greater than the displacement bound.
     */
    public static Pair<Double> getLongitudeBounds(Location Location, int displacementBound) {

        // TODO: Scrutinize further if the formula is correct or not.
        double deltaLongitude = displacementBound / (EARTH_RADIUS * Math.cos(Location.getLatitude())) * 360 / (2 * Math.PI);
        return new Pair<Double>(Location.getLongitude() - deltaLongitude, Location.getLongitude() + deltaLongitude);
    }

    /**
     * Pair utility class
     *
     * @param <T>
     */
    public static class Pair<T> {

        /**
         * First element in pair
         */
        private T first;

        /**
         * Second element in pair
         */
        private T second;

        public Pair(T first, T second) {
            this.first = first;
            this.second = second;
        }

        public T getFirst() {
            return first;
        }

        public void setFirst(T first) {
            this.first = first;
        }

        public T getSecond() {
            return second;
        }

        public void setSecond(T second) {
            this.second = second;
        }
    }

    /**
     * Displacement comparator for {@link Location}s. Displacements are measured from a reference.
     */
    public static class DisplacementComparator implements Comparator<Warehouse> {

        /**
         * Location of reference from which displacement will be measured
         */
        private Location reference;

        public DisplacementComparator(Location reference) {
            this.reference = reference;
        }

        public int compare(Warehouse w1, Warehouse w2) {
            return displacement(w1.readLocation(), reference) - displacement(w2.readLocation(), reference);
        }
    }
}
