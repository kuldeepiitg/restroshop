package com.restroshop.dao;

import com.restroshop.dao.model.Location;
import com.restroshop.dao.model.Restaurant;
import com.restroshop.dao.model.Warehouse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.Serializable;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Junit test for {@link WarehouseDaoImpl}.
 * <p>
 * Created by Kuldeep Yadav on 21/07/16.
 */
public class WarehouseDaoImplTest {

    private EntityDao<Warehouse> dao;

    @Before
    public void setUp() throws Exception {
        this.dao = new WarehouseDaoImpl();
    }

    @After
    public void tearDown() throws Exception {
    }

    public Warehouse createWarehouse() {
        Warehouse warehouse = new Warehouse();
        warehouse.setName("Mysore");
        warehouse.setLatitude(12.2958);
        warehouse.setLongitude(76.6394);
        warehouse.setRadius(20);
        return warehouse;
    }

    @Test
    public void create() throws Exception {
        Warehouse warehouse = createWarehouse();
        Serializable id = dao.create(warehouse);
        Warehouse warehouseInDB = dao.read(id);
        assertEquals(warehouse.getId(), warehouseInDB.getId());
        assertEquals(warehouse.getRadius(), warehouseInDB.getRadius());
        assertEquals(warehouse.getLatitude(), warehouseInDB.getLatitude(), 0.0001);
        assertEquals(warehouse.getLongitude(), warehouseInDB.getLongitude(), 0.0001);
    }

    @Test
    public void read() throws Exception {
        // read is covered in create()
        create();
    }

    @Test
    public void list() throws Exception {
        dao.create(createWarehouse());
        List<Warehouse> list = dao.list();
        assertTrue(list.size() > 0);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void list1() throws Exception {
        dao.create(createWarehouse());
        dao.list(0, 1);
    }

    @Test
    public void update() throws Exception {
        Warehouse warehouse = createWarehouse();
        Serializable id = dao.create(warehouse);
        int radius = warehouse.getRadius() + 10;
        warehouse.setRadius(radius);
        dao.update(warehouse);
        Warehouse updatedWarehouse = dao.read(id);
        assertEquals(radius, updatedWarehouse.getRadius());
    }

    @Test
    public void delete() throws Exception {
        Warehouse warehouse = createWarehouse();
        Serializable id = dao.create(warehouse);
        dao.delete(id);
        Warehouse warehouseInDB = dao.read(id);
        assertNull(warehouseInDB);
    }

    @Test
    public void findNearBy() throws Exception {
        Warehouse mgRoadWarehouse = new Warehouse();
        mgRoadWarehouse.setName("MG Road Warehouse");
        mgRoadWarehouse.setLocation(new Location(13.9756, 77.6066));
        Serializable id1 = dao.create(mgRoadWarehouse);

        Warehouse hebbalWarehouse = new Warehouse();
        hebbalWarehouse.setName("Hebbal Warehouse");
        hebbalWarehouse.setLocation(new Location(14.0358, 77.5970));
        Serializable id2 = dao.create(hebbalWarehouse);

        Restaurant kormanglaRestaurant = RestaurantDaoImplTest.createRestaurant();
        kormanglaRestaurant.setLocation(new Location(13.9279, 77.6271));

        List<Warehouse> warehouses = ((WarehouseDaoImpl) dao).findNearBy(kormanglaRestaurant.readLocation(), 10 * 1000);
        assertEquals(1, warehouses.size());
        assertEquals(mgRoadWarehouse.getLatitude(), warehouses.get(0).getLatitude(), 0.00001);
        assertEquals(mgRoadWarehouse.getLongitude(), warehouses.get(0).getLongitude(), 0.00001);
        dao.delete(id1);
        dao.delete(id2);
    }

}