package com.restroshop.dao;

import com.restroshop.dao.model.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Junit test for {@link SessionDaoImpl}
 * <p>
 * Created by Kuldeep Yadav on 01/08/16.
 */
public class SessionDaoImplTest {

    static Random random = new Random(System.currentTimeMillis());
    private EntityDao<Session> dao;

    public static Session createSession() {
        int randomNumber = random.nextInt();
        return new Session(String.valueOf(randomNumber), 10);
    }

    @Before
    public void setUp() throws Exception {
        dao = new SessionDaoImpl();
        random = new Random(System.currentTimeMillis());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void create() throws Exception {
        Session session = createSession();
        long sessionId = (Long) dao.create(session);
        Session sessionInDB = dao.read(sessionId);
        assertEquals(session.getId(), sessionInDB.getId());
        assertEquals(session.getImei(), sessionInDB.getImei());
        assertEquals(session.getUserId(), sessionInDB.getUserId());
    }

    @Test
    public void read() throws Exception {
        // covered by read
        create();
    }

    @Test
    public void readByIMEI() throws Exception {

        int randomNumber = random.nextInt();
        Session session = createSession();
        session.setImei(String.valueOf(randomNumber));
        dao.create(session);
        Session sessionInDB = ((SessionDaoImpl) dao).read(String.valueOf(randomNumber));
        assertEquals(session.getId(), sessionInDB.getId());
        assertEquals(session.getImei(), sessionInDB.getImei());
        assertEquals(session.getUserId(), sessionInDB.getUserId());
    }

    @Test
    public void list() throws Exception {
        create();
        List<Session> list = dao.list();
        assertTrue(list.size() > 0);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void listByPage() throws Exception {
        dao.list(0, 10);
    }

    @Test
    public void update() throws Exception {
        Session session = createSession();
        long sessionId = (Long) dao.create(session);
        Session sessionInDB = dao.read(sessionId);

        long newDeviceId = 10000;
        sessionInDB.setUserId(newDeviceId);
        dao.update(sessionInDB);

        Session updatedSessionInDB = dao.read(sessionId);
        assertEquals(newDeviceId, updatedSessionInDB.getUserId());
    }

    @Test
    public void delete() throws Exception {
        Session session = createSession();
        long sessionId = (Long) dao.create(session);

        dao.delete(sessionId);
        Session deletedSession = dao.read(sessionId);
        assertNull(deletedSession);
    }
}