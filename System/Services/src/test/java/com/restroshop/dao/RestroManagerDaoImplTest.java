package com.restroshop.dao;

import com.restroshop.dao.model.Restaurant;
import com.restroshop.dao.model.RestroManager;
import org.junit.After;
import org.junit.Before;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Junit test for {@link RestroManagerDaoImpl}
 * <p/>
 * Created by Kuldeep Yadav on 28-Jun-16.
 */
public class RestroManagerDaoImplTest {

    private EntityDao<Restaurant> restaurantDao;
    private EntityDao<RestroManager> dao;

    public static RestroManager createRestaurantManager() {
        RestroManager manager = new RestroManager();
        manager.setName("Kuldeep");
        manager.setEmail("kuldeep.writes@gmail.com");
        manager.setPhone("9535580366");
        manager.setPicUrl("pic_kd.png");
        manager.setPassword("password");
        return manager;
    }

    @Before
    public void setUp() throws Exception {
        restaurantDao = new RestaurantDaoImpl();
        dao = new RestroManagerDaoImpl();
    }

    @After
    public void tearDown() throws Exception {
    }

    //    @Test
    public void testCreate() throws Exception {

        Restaurant restaurant = RestaurantDaoImplTest.createRestaurant();
        Long restaurantId = (Long) restaurantDao.create(restaurant);
        RestroManager manager = createRestaurantManager();
        Long restaurantManagerId = (Long) dao.create(manager);

        restaurantDao.update(restaurant);
    }

    //    @Test
    public void testRead() throws Exception {
        testCreate();
    }

    //    @Test
    public void testList() throws Exception {
        testCreate();
        List<RestroManager> restroManagers = dao.list();
        assertNotEquals(0, restroManagers.size());
    }

    //    @Test
    public void testUpdate() throws Exception {
        RestroManager manager = createRestaurantManager();
        dao.create(manager);
        String updatedPhoneNumber = "+91 9416294162";
        manager.setPhone(updatedPhoneNumber);
        dao.update(manager);
        assertEquals(updatedPhoneNumber, manager.getPhone());
    }

    //    @Test
    public void testDelete() throws Exception {
        RestroManager manager = createRestaurantManager();
        Long restaurantManagerId = (Long) dao.create(manager);
        dao.delete(restaurantManagerId);
        RestroManager managerInDB = dao.read(restaurantManagerId);
        assertNull(managerInDB);
    }

}