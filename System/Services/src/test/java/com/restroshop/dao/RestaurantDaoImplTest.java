package com.restroshop.dao;

import com.restroshop.dao.model.Restaurant;
import com.restroshop.dao.model.transiant.Address;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Junit test cases for {@link RestaurantDaoImpl}
 * <p/>
 * Created by Kuldeep Yadav on 27-Jun-16.
 */
public class RestaurantDaoImplTest {

    private static Random random = new Random(System.currentTimeMillis());
    private EntityDao<Restaurant> dao;

    public static Address createAddress() {
        Address address = new Address();
        address.setComplexName("Punjabi Rasoi");
        address.setArea("R.T. Nagar");
        address.setCity("Bangalore");
        address.setState("Karnataka");
        address.setPincode("560024");
        return address;
    }

    public static Restaurant createRestaurant() {
        int randomNumber = random.nextInt();
        Restaurant restaurant = new Restaurant();
        restaurant.setName("Punjabi Rasoi");
        restaurant.setLatitude(23.4);
        restaurant.setLongitude(54.3);
        restaurant.setPicUrl("restaurant/punjabirasoi.png");
        restaurant.setEmail("contact" + randomNumber +
                "@punjabirasoi.com");
        restaurant.setPhone("9575825862");
        return restaurant;
    }

    @Before
    public void setUp() throws Exception {
        dao = new RestaurantDaoImpl();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCreate() throws Exception {
        Restaurant restaurant = createRestaurant();
        Address address = createAddress();
        restaurant.setAddress(address);
        long restaurantId = (Long) dao.create(restaurant);
        Restaurant restaurantInDB = dao.read(restaurantId);
        assertEquals(restaurant.getName(), restaurantInDB.getName());
        assertEquals(restaurant.getPicUrl(), restaurantInDB.getPicUrl());
        assertEquals(address.getPincode(), restaurantInDB.readAddress().getPincode());
        assertEquals(address.getState(), restaurantInDB.readAddress().getState());
        assertEquals(address.getCity(), restaurantInDB.readAddress().getCity());
        assertEquals(address.getArea(), restaurantInDB.readAddress().getArea());
        double deltaError = 0.0001;
        assertEquals(restaurant.getLatitude(), restaurantInDB.getLatitude(), deltaError);
        assertEquals(restaurant.getLongitude(), restaurantInDB.getLongitude(), deltaError);
    }

    @Test
    public void testRead() throws Exception {
        testCreate();
    }

    @Test
    public void testList() throws Exception {
        testCreate();
        List<Restaurant> list = dao.list();
        assertTrue(list.size() > 0);
    }

    @Test
    public void testUpdate() throws Exception {

        Restaurant restaurant = createRestaurant();
        long restaurantId = (Long) dao.create(restaurant);
        Restaurant restaurantInDB = dao.read(restaurantId);

        String updatedName = "Punjabi Tadka";
        restaurantInDB.setName(updatedName);
        dao.update(restaurantInDB);

        Restaurant updatedRestaurantInDB = dao.read(restaurantId);
        assertEquals(updatedName, updatedRestaurantInDB.getName());
    }

    @Test
    public void testDelete() throws Exception {

        Restaurant restaurant = createRestaurant();
        long restaurantId = (Long) dao.create(restaurant);

        dao.delete(restaurantId);
        Restaurant deletedRestaurant = dao.read(restaurantId);
        assertNull(deletedRestaurant);
    }

    @Test
    public void testReadByEmail() throws Exception {
        Restaurant restaurant = createRestaurant();
        dao.create(restaurant);

        Restaurant restaurantInDB = ((RestaurantDaoImpl) dao).readByEmail(restaurant.getEmail());
        assertEquals(restaurant.getId(), restaurantInDB.getId());
        assertEquals(restaurant.getEmail(), restaurantInDB.getEmail());
    }

    @Test
    public void readSellingRates() throws Exception {
        // TODO: 03/08/16 implement
    }

    @Test
    public void readSellingRates1() throws Exception {
        // TODO: 03/08/16 implement
    }
}