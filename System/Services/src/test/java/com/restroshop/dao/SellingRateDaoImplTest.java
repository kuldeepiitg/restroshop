package com.restroshop.dao;

import com.restroshop.dao.model.Article;
import com.restroshop.dao.model.Restaurant;
import com.restroshop.dao.model.SellingRate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Junit test for {@link SaleRateDaoImpl}
 * <p/>
 * Created by Kuldeep Yadav on 27-Jun-16.
 */
public class SellingRateDaoImplTest {

    private EntityDao<Article> articleDao;
    private EntityDao<Restaurant> restaurantDao;
    private EntityDao<SellingRate> dao;
    private Random random;

    @Before
    public void setUp() throws Exception {
        articleDao = new ArticleDaoImpl();
        restaurantDao = new RestaurantDaoImpl();
        dao = new SaleRateDaoImpl();
        random = new Random(System.currentTimeMillis());
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testCreate() throws Exception {
        Article article = ArticleDaoImplTest.createArticle();
        Restaurant restaurant = RestaurantDaoImplTest.createRestaurant();

        articleDao.create(article);
        restaurantDao.create(restaurant);

        SellingRate rate = new SellingRate();
        rate.setPrice(1000);
        rate.setArticle(article);
        rate.setSalePoint(restaurant);
        rate.setStartDate((new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000)));
        rate.setEndDate(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
        dao.create(rate);

        // Refreshing entity will make hibernate synchronize object with database

        SellingRate id = new SellingRate(article, restaurant, -1,
                new Date(System.currentTimeMillis()),
                new Date(System.currentTimeMillis()));
        SellingRate rateInDB = dao.read(id);
        assertNotNull(rateInDB);
        assertEquals(rate.getPrice(), rateInDB.getPrice());
        assertEquals(rate.getArticle(), rateInDB.getArticle());
        assertEquals(rate, rateInDB);
    }

    @Test
    public void testRead() throws Exception {
        Article article = ArticleDaoImplTest.createArticle();
        Restaurant restaurant = RestaurantDaoImplTest.createRestaurant();

        articleDao.create(article);
        restaurantDao.create(restaurant);

        SellingRate rate = new SellingRate();
        rate.setPrice(1000);
        rate.setArticle(article);
        rate.setSalePoint(restaurant);
        rate.setStartDate((new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000)));
        rate.setEndDate(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
        Serializable id = dao.create(rate);

        SellingRate rateInDB = dao.read(id);
        assertEquals(1000, rateInDB.getPrice());
    }

    @Test
    public void testList() throws Exception {
        testCreate();
        List<SellingRate> list = dao.list();
        assertNotEquals(0, list.size());
    }

    @Test
    public void testUpdate() throws Exception {
        Article article = ArticleDaoImplTest.createArticle();
        Restaurant restaurant = RestaurantDaoImplTest.createRestaurant();

        articleDao.create(article);
        restaurantDao.create(restaurant);

        SellingRate rate = new SellingRate();
        rate.setPrice(1000);
        rate.setArticle(article);
        rate.setSalePoint(restaurant);
        rate.setStartDate((new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000)));
        rate.setEndDate(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
        Serializable id =  dao.create(rate);

        rate.setPrice(100);
        dao.update(rate);

        SellingRate rateInDB = dao.read(id);
        assertEquals(100, rateInDB.getPrice());
    }

    @Test
    public void testDelete() throws Exception {
        Article article = ArticleDaoImplTest.createArticle();
        Restaurant restaurant = RestaurantDaoImplTest.createRestaurant();

        articleDao.create(article);
        restaurantDao.create(restaurant);

        SellingRate rate = new SellingRate();
        rate.setPrice(1000);
        rate.setArticle(article);
        rate.setSalePoint(restaurant);
        rate.setStartDate((new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000)));
        rate.setEndDate(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
        Serializable id = dao.create(rate);

        dao.delete(id);
        assertNull(dao.read(id));
    }

    @Test
    public void read() throws Exception {
        Article article = ArticleDaoImplTest.createArticle();
        Restaurant restaurant = RestaurantDaoImplTest.createRestaurant();

        articleDao.create(article);
        restaurantDao.create(restaurant);

        SellingRate rate = new SellingRate();
        rate.setPrice(1000);
        rate.setArticle(article);
        rate.setSalePoint(restaurant);
        rate.setStartDate((new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000)));
        rate.setEndDate(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
        dao.create(rate);

        SellingRate rateInDB = ((SaleRateDaoImpl) dao).read(article.getId(), restaurant.getId(), new Date(System.currentTimeMillis()));
        assertEquals(rate.getPrice(), rateInDB.getPrice());
        assertEquals(rate.getStartDate().toString(), rateInDB.getStartDate().toString());
        assertEquals(rate.getEndDate().toString(), rateInDB.getEndDate().toString());
    }

    //    @Test
    public void fillTable() {
        Restaurant restaurant = restaurantDao.read(65L);
        for (int i = 1; i <= 1858; i++) {
            Article article = articleDao.read(Long.valueOf(i));
            if (article == null) continue;
            SellingRate rate = new SellingRate();
            rate.setArticle(article);
            rate.setSalePoint(restaurant);
            rate.setPrice(Math.abs(random.nextInt()) % 10000);
            rate.setStartDate(new Date(System.currentTimeMillis() - 24 * 3600 * 1000));
            rate.setEndDate(new Date(System.currentTimeMillis() + 5 * 24 * 3600 * 1000));
            dao.create(rate);
        }
    }

    //    @Test
    public void test() throws SQLException, IOException, ClassNotFoundException {
        long start = System.currentTimeMillis();
        ArrayList<Long> salepointIds = new ArrayList<Long>();
        salepointIds.add(2L);
        salepointIds.add(1L);
        List<SaleRateDaoImpl.SellingRateRow> list = ((SaleRateDaoImpl) dao).listRows(salepointIds, new Date(System.currentTimeMillis()));
        System.out.println(list.size());
        System.out.println(System.currentTimeMillis() - start);
    }
}