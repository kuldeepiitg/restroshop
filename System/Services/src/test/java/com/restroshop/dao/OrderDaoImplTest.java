package com.restroshop.dao;

import com.restroshop.dao.model.BasketItem;
import com.restroshop.dao.model.Order;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Junit test for {@link OrderDaoImpl}
 * <p>
 * Created by Kuldeep Yadav on 13/08/16.
 */
public class OrderDaoImplTest {

    private OrderDaoImpl orderDao;

    @Before
    public void setUp() throws Exception {
        orderDao = new OrderDaoImpl();
    }

    @After
    public void tearDown() throws Exception {

    }

    public Order createOrder() {
        Order order = new Order();
        order.setContactPerson("Kuldeep Yadav");
        order.setPhone("9535580366");
        order.setArea("Hebbal");
        order.setCity("Bangalore");
        order.setPincode("560024");
        order.setRestaurantName("Punjabi Rasoi");
        order.setState("Karnataka");
        order.setStreet("1st cross");
        order.setComplex("Anugraha");

        Set<BasketItem> items = new HashSet<BasketItem>();
        items.add(new BasketItem(1L, "tomato", "KG", 100, 2));
        items.add(new BasketItem(2L, "apple", "KG", 200, 3));

        order.setItems(items);
        return order;
    }

    @Test
    public void create() throws Exception {
        Order order = createOrder();
        Serializable id = orderDao.create(order);
        Order orderInDB = orderDao.read(id);
        assertEquals(order.getContactPerson(), orderInDB.getContactPerson());
        assertEquals(order.getArea(), orderInDB.getArea());
        assertEquals(order.getCity(), orderInDB.getCity());
        assertEquals(order.getItems().size(), orderInDB.getItems().size());
    }

    @Test
    public void read() throws Exception {
        create();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void list() throws Exception {
        orderDao.list();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void list1() throws Exception {
        orderDao.list(0, 10);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void update() throws Exception {
        Order order = createOrder();
        Serializable id = orderDao.create(order);
        Order orderInDB = orderDao.read(id);
        orderInDB.setCity("Delhi");
        orderDao.update(orderInDB);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void delete() throws Exception {
        Order order = createOrder();
        Serializable id = orderDao.create(order);
        orderDao.delete(id);
    }
}