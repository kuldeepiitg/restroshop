package com.restroshop.dao;

import com.restroshop.authentication.AuthUtils;
import com.restroshop.dao.model.User;
import org.junit.After;
import org.junit.Before;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Junit test for {@link UserDaoImpl}
 * <p>
 * Created by Kuldeep Yadav on 30-Jun-16.
 */
public class UserDaoImplTest {

    private EntityDao<User> dao;

    public static User createUser() {
        User user = new User();
        user.setName("Rishika Yadav");
        user.setPhone("+91 9535580366");
        user.setPicUrl("rishika_pic.png");
        user.setEmail("rishika.writes@gmail.com");
        user.setRole(User.Role.RESTRO_MANAGER);
        user.setPassword(AuthUtils.checksum("password", AuthUtils.ChecksumType.MD5));
        return user;
    }

    @Before
    public void setUp() throws Exception {
        this.dao = new UserDaoImpl();
    }

    @After
    public void tearDown() throws Exception {
        ((UserDaoImpl) dao).shutdown();
    }

    //    @Test
    public void testCreate() throws Exception {
        User user = createUser();
        dao.create(user);

        User userInDB = dao.read(user.getId());
        assertEquals(user.getName(), userInDB.getName());
        assertEquals(user.getEmail(), userInDB.getEmail());
        assertEquals(user.getRole(), userInDB.getRole());
        assertEquals(user.getPassword(), userInDB.getPassword());
        assertEquals(user.getId(), userInDB.getId());
        assertEquals(user.getPhone(), userInDB.getPhone());
        assertEquals(user.getPicUrl(), userInDB.getPicUrl());
    }

    //    @Test
    public void testRead() throws Exception {
        // covered in create
        testCreate();
    }

    //    @Test
    public void testList() throws Exception {
        testCreate();
        List<User> list = dao.list();
        assertNotEquals(0, list.size());
    }

    //    @Test
    public void testUpdate() throws Exception {
        User user = createUser();
        dao.create(user);

        User userInDB = dao.read(user.getId());
        userInDB.setRole(User.Role.ADMINISTRATOR);
        dao.update(userInDB);

        User updatedUserInDB = dao.read(user.getId());
        assertEquals(userInDB.getRole(), updatedUserInDB.getRole());
    }

    //    @Test
    public void testDelete() throws Exception {

        User user = createUser();
        dao.create(user);
        dao.delete(user.getId());
        User deletedUser = dao.read(user.getId());
        assertNull(deletedUser);
    }

    //    @Test
    public void testRead1() throws Exception {
        User user = new User();
        user.setName("Rishika Yadav");
        user.setPhone("+91 9535580366");
        user.setPicUrl("rishika_pic.png");

        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        int i = random.nextInt();

        user.setEmail("rishika.writes" + i + "@gmail.com");
        user.setRole(User.Role.RESTRO_MANAGER);
        user.setPassword(AuthUtils.checksum("password", AuthUtils.ChecksumType.MD5));

        dao.create(user);
        User userInDB = dao.read(user.getEmail());
        assertEquals(user.getName(), userInDB.getName());
        assertEquals(user.getEmail(), userInDB.getEmail());
    }
}