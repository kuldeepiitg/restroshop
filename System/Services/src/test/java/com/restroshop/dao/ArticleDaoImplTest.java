package com.restroshop.dao;

import com.restroshop.dao.model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Junit test for {@link ArticleDaoImpl}
 * <p/>
 * Created by Kuldeep Yadav on 24-Jun-16.
 */
public class ArticleDaoImplTest {

    private static Random random = new Random(System.currentTimeMillis());
    private EntityDao<Article> dao;
    private EntityDao<Restaurant> restaurantDao;
    private EntityDao<SellingRate> rateDao;

    public static Article createArticle() {
        Article article = new Article();
        if(random.nextInt()%3 == 0) {
            article.setName("Tomato");
            article.setDescription("Achhe taaza tamatar le lo");
            article.setPicUrl("tomato.png");
        } else if(random.nextInt()%3 == 1) {
            article.setName("Carrot");
            article.setDescription("Achhe taaza gazar le lo");
            article.setPicUrl("carrot.png");
        } else {
            article.setName("Apple");
            article.setDescription("Achhe taaza seb le lo");
            article.setPicUrl("apple.png");
        }
        article.setUnit(Unit.KG);
        return article;
    }

    @Before
    public void setUp() throws Exception {
        dao = new ArticleDaoImpl();
        restaurantDao = new RestaurantDaoImpl();
        rateDao = new SaleRateDaoImpl();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testCreate() throws Exception {
        Article article = createArticle();
        long articleId = (Long) dao.create(article);

        Article articleInDB = dao.read(articleId);
        assertEquals(article.getName(), articleInDB.getName());
        assertEquals(article.getDescription(), articleInDB.getDescription());
        assertEquals(article.getPicUrl(), articleInDB.getPicUrl());
        assertEquals(article.getUnit(), articleInDB.getUnit());
    }

    @Test
    public void testRead() throws Exception {
        // covered in testCreate
        testCreate();
    }

    @Test
    public void testList() throws Exception {
        testCreate();
        List<Article> list = dao.list();
        assertTrue(list.size() > 0);
    }

    @Test
    public void list() throws Exception {
        for (int i = 0; i < 20; i++) {
            testCreate();
        }
        List<Article> list = dao.list(5, 10);
        assertEquals(10, list.size());
    }

    @Test
    public void testUpdate() throws Exception {

        Article article = createArticle();
        long articleId = (Long) dao.create(article);
        Article articleInDB = dao.read(articleId);

        assertEquals(article.getUnit(), articleInDB.getUnit());
        articleInDB.setUnit(Unit.GRAM);
        dao.update(articleInDB);

        Article updatedArticleInDB = dao.read(articleId);
        assertEquals(Unit.GRAM, updatedArticleInDB.getUnit());
    }

    @Test
    public void testDelete() throws Exception {

        Article article = createArticle();
        long articleId = (Long) dao.create(article);

        dao.delete(articleId);
        Article deletedArticle = dao.read(articleId);
        assertNull(deletedArticle);
    }

    @Test
    public void testGetSalePointArticle() throws Exception {
        Article article = ArticleDaoImplTest.createArticle();
        Restaurant restaurant = RestaurantDaoImplTest.createRestaurant();

        Long articleId = (Long) dao.create(article);
        Long restaurantId = (Long) restaurantDao.create(restaurant);

        SellingRate rate = new SellingRate();
        Random random = new Random(System.currentTimeMillis());
        int price = random.nextInt(10000);
        rate.setPrice(price);
        rate.setArticle(article);
        rate.setSalePoint(restaurant);
        Date date = new Date(System.currentTimeMillis());
        rate.setStartDate(new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000));
        rate.setEndDate(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
        rateDao.create(rate);

        SalePointArticle salePointArticle = ((ArticleDaoImpl) dao).getSalePointArticle(articleId, restaurantId, date);
        assertEquals(article.getId(), salePointArticle.getId());
        assertEquals(rate.getPrice(), salePointArticle.getRate());
    }

    //    @Test
    public void replicateTable() {
        List<Article> list = dao.list();
        for (Article article : list) {
            Article copy = new Article();
            copy.setName(article.getName());
            copy.setUnit(article.getUnit());
            copy.setPicUrl(article.getPicUrl());
            copy.setDescription(article.getDescription());
            dao.create(copy);
        }
    }

    @Test
    public void testListNames() throws Exception {
        List<String> names = ((ArticleDaoImpl) dao).names();
        assertTrue(names.size() > 0);
    }

    @Test
    public void listFresh() throws Exception {
        Article article = createArticle();
        dao.create(article);
        Thread.sleep(1000);
        long timestamp = System.currentTimeMillis();
        Thread.sleep(1000);
        article = createArticle();
        dao.create(article);
        List<Article> list = ((ArticleDaoImpl) dao).list(new Timestamp(timestamp));
        assertEquals(1, list.size());
    }
}