package com.restroshop.dao;

import com.restroshop.dao.model.Article;
import com.restroshop.dao.model.PurchaseRate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Junit test for {@link PurchaseRateDaoImpl}
 * <p>
 * Created by Kuldeep Yadav on 11-Jul-16.
 */
public class PurchaseRateDaoImplTest {

    private EntityDao<PurchaseRate> dao;
    private EntityDao<Article> articleEntityDao;

    @Before
    public void setUp() throws Exception {
        dao = new PurchaseRateDaoImpl();
        articleEntityDao = new ArticleDaoImpl();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCreate() throws Exception {
        Article article = ArticleDaoImplTest.createArticle();
        articleEntityDao.create(article);

        PurchaseRate rate = new PurchaseRate();
        rate.setArticle(article);
        rate.setPrice(11);

        dao.create(rate);

        PurchaseRate rateInDB = dao.read(rate.getId());
        assertEquals(rate.getPrice(), rateInDB.getPrice());
        assertEquals(rate.getId(), rateInDB.getId());
    }

    @Test
    public void testRead() throws Exception {
        // Covered in testCreate
        testCreate();
    }

    @Test
    public void testList() throws Exception {

    }

    @Test
    public void testList1() throws Exception {

    }

    @Test
    public void testUpdate() throws Exception {
        Article article = ArticleDaoImplTest.createArticle();
        articleEntityDao.create(article);

        PurchaseRate rate = new PurchaseRate();
        rate.setArticle(article);
        rate.setPrice(11);

        dao.create(rate);
        PurchaseRate rateInDB = dao.read(rate.getId());
        rateInDB.setPrice(123);

        dao.update(rateInDB);
        assertEquals(rateInDB.getId(), dao.read(rate.getId()).getId());
    }

    @Test
    public void testDelete() throws Exception {
        Article article = ArticleDaoImplTest.createArticle();
        articleEntityDao.create(article);

        PurchaseRate rate = new PurchaseRate();
        rate.setArticle(article);
        rate.setPrice(11);
        dao.create(rate);

        dao.delete(rate.getId());
        PurchaseRate rateInDB = dao.read(rate.getId());
        assertNull(rateInDB);
    }
}